﻿namespace LogInterface
{
    /// <summary>
    /// Logging levels.
    /// </summary>
    public enum LogLevel
    {
        /// <summary>
        /// The most accurate level.
        /// </summary>
        Trace,
        /// <summary>
        /// Information level.
        /// </summary>
        Info,
        /// <summary>
        /// Level for errors logging.
        /// </summary>
        Error
    }
}
