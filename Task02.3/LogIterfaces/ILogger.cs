﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LogInterface
{
    /// <summary>
    /// Basic logger functionality interface.
    /// </summary>
    public interface ILogger
    {
        /// <summary>
        /// Logger instance name.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Load logger settings from app.config. Recommended to use after .ctor in try-catch.
        /// </summary>
        void LoadSettings();

        /// <summary>
        /// Log a message.
        /// </summary>
        /// <param name="level">Level of logging.</param>
        /// <param name="message">Message to be logged.</param>
        void Log(LogLevel level, string message);

        /// <summary>
        /// Log dictionary of parameters on trace level.
        /// </summary>
        /// <param name="parameters">Parameters to be logged.</param>
        void Trace(Dictionary<string, object> parameters)
        {
            StringBuilder message = new();

            foreach (KeyValuePair<string, object> pair in parameters)
            {
                message.Append($"{pair.Key} = {pair.Value}, ");
            }

            Log(LogLevel.Trace, message
                .Remove(message.Length - 2, 2).ToString());
        }

        /// <summary>
        /// Log message on info level.
        /// </summary>
        /// <param name="message">Message to be logged.</param>
        void Info(string message) => Log(LogLevel.Info, message);

        /// <summary>
        /// Log exception on error level.
        /// </summary>
        /// <param name="exception">Exception to be logged.</param>
        void Error(Exception exception)
        {
            Log(LogLevel.Error,
                $"An {exception.GetType()} occurred on {exception.Source}.\n" +
                $"Exception message: {exception.Message}.\n" +
                $"Stack trace: {exception.StackTrace}");
        }
    }
}
