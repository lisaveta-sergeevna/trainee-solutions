﻿namespace LogInterface
{
    /// <summary>
    /// Factory method interface for ILogger.
    /// </summary>
    public interface ILogFactory
    {
        /// <summary>
        /// Optional report about logger creation.
        /// </summary>
        public string CreationReport { get; }

        /// <summary>
        /// Creation options, required.
        /// </summary>
        public LogFactoryOptions Options { get; set; }

        /// <summary>
        /// Logger creation method.
        /// </summary>
        /// <returns>Logger created by specified options.</returns>
        ILogger CreateLogger();
    }
}
