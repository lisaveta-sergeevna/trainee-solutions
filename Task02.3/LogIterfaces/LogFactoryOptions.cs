﻿using System;

namespace LogInterface
{
    /// <summary>
    /// Data source options for LogFactory that should be provided by client.
    /// </summary>
    public class LogFactoryOptions
    {
        /// <summary>
        /// Assembly to search logger into.
        /// </summary>
        public string Assembly { get; set; } = System.Reflection.Assembly
            .GetExecutingAssembly().FullName;

        /// <summary>
        /// Concrete type of logger from Assembly that should be created.
        /// </summary>
        public string Type { get; }

        /// <summary>
        /// Name of logger to create to.
        /// </summary>
        public string Name { get; set; } = "logger";

        /// <summary>
        /// Minimum level of logging to set.
        /// </summary>
        public LogLevel MinLevel { get; set; } = LogLevel.Trace;

        /// <summary>
        /// Constructs log factory options, type is required element.
        /// </summary>
        /// <param name="type">Type of logger to create to.</param>
        /// <exception cref="ArgumentNullException">Thrown when type is null.</exception>
        public LogFactoryOptions(string type)
        {
            if (type is null)
            {
                throw new ArgumentNullException("Logger type " +
                                                "should be specified " +
                                                "as not null for loggers " +
                                                "creation.");
            }

            Type = type;
        }
    }
}
