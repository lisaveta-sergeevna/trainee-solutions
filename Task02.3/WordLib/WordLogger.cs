﻿using System;
using System.IO;
using System.Linq;
using LogInterface;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using System.Configuration;

namespace WordLib
{
    /// <summary>
    /// Log information into word document.
    /// </summary>
    public class WordLogger : ILogger
    {
        private readonly string _logFileKey = "logFile";

        /// <summary>
        /// Name of the logger.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Name of the document to log info to.
        /// </summary>
        public string LogFile { get; private set; } = "log.docx";

        /// <summary>
        /// Minimum level of logging.
        /// </summary>
        public LogLevel MinLevel { get; }

        /// <summary>
        /// Constructs logger with properties.
        /// </summary>
        /// <param name="name">Logger name.</param>
        /// <param name="minLevel">Minimum level of logging.</param>
        public WordLogger(string name, LogLevel minLevel)
        {
            Name = name;
            MinLevel = minLevel;
        }

        /// <summary>
        /// Load logger settings from app.config. Recommended to use after .ctor in try-catch.
        /// </summary>
        public void LoadSettings()
        {
            AppSettingsSection appSettings = ConfigurationManager
                .OpenExeConfiguration(GetType().Namespace + ".dll").AppSettings;

            if (appSettings is not null && appSettings.Settings
                .AllKeys.Contains(_logFileKey))
            {
                string format = appSettings.Settings[_logFileKey].Value;
                LogFile = string.Format(format, DateTime.Now);
            }
        }

        /// <summary>
        /// Log message to document on the level.
        /// </summary>
        /// <param name="level">Level to log to.</param>
        /// <param name="message">Message to log to.</param>
        /// <exception cref="ArgumentException">Thrown when current level is unavailable.</exception>
        public void Log(LogLevel level, string message)
        {
            if (MinLevel <= level)
            {
                string entry = $"{DateTime.Now:R} | {Name ?? "Logger"} | {level} | {message}";

                bool exists = File.Exists(LogFile);

                using WordprocessingDocument document = exists ? 
                    WordprocessingDocument.Open(LogFile, true) : 
                    WordprocessingDocument.Create(LogFile, WordprocessingDocumentType.Document);

                MainDocumentPart mainPart = exists ? 
                    document.MainDocumentPart : 
                    document.AddMainDocumentPart();

                mainPart.Document ??= new Document();

                Body body = mainPart.Document.AppendChild(new Body());
                Paragraph paragraph = body.AppendChild(new Paragraph());
                Run run = paragraph.AppendChild(new Run());

                run.AppendChild(new Text(entry));
            }
            else
            {
                throw new ArgumentException($"Current level {level} " +
                                            $"is unavailable.");
            }
        }
    }
}
