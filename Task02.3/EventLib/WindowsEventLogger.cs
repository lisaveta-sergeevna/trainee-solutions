﻿using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using LogInterface;

namespace EventLib
{
    /// <summary>
    /// Log information into windows event log.
    /// </summary>
    public class WindowsEventLogger : ILogger
    {
        private readonly string _source = "Application";

        /// <summary>
        /// Logger name.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Minimum level of logging.
        /// </summary>
        public LogLevel MinLevel { get; }

        /// <summary>
        /// Constructor of event listener.
        /// </summary>
        /// <param name="name">Name of logger.</param>
        /// <param name="minLevel">Minimum logging value.</param>
        public WindowsEventLogger(string name, LogLevel minLevel)
        {
            Name = name;
            MinLevel = minLevel;
        }

        /// <summary>
        /// Load logger settings from app.config. Recommended to use after .ctor in try-catch.
        /// </summary>
        public void LoadSettings()
        { }

        /// <summary>
        /// Log message to windows event log on mentioned level.
        /// </summary>
        /// <param name="level">Level to log.</param>
        /// <param name="message">Message to log.</param>
        /// <exception cref="ArgumentException">Thrown when level is
        /// unavailable for this logger.</exception>
        [SuppressMessage("Interoperability", 
            "CA1416:Validate platform compatibility", 
            Justification = "<Pending>")]
        public void Log(LogLevel level, string message)
        {
            if (MinLevel <= level)
            {
                using EventLog eventLog = new(_source)
                {
                    Source = _source
                };

                EventLogEntryType type = level switch
                {
                    LogLevel.Trace => EventLogEntryType.SuccessAudit,
                    LogLevel.Info => EventLogEntryType.Information,
                    _ => EventLogEntryType.Error
                };

                string entry = $"{Name ?? "Log"}, {level} : {DateTime.Now:R}\n" +
                               $"Message : {message}\n";

                eventLog.WriteEntry(entry, type);
            }
            else
            {
                throw new ArgumentException($"Current level {level}" +
                                            $" is unavailable.");
            }
        }
    }
}
