﻿using System;

namespace LogLib.Attributes
{
    /// <summary>
    /// Attribute to identify tracked properties and fields.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class TrackingPropertyAttribute : Attribute
    {
        /// <summary>
        /// Name of tracked item.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Constructor of the attribute with specified name.
        /// </summary>
        /// <param name="name">Name to set into name property of the attribute.</param>
        public TrackingPropertyAttribute(string name = null) => Name = name;
    }
}
