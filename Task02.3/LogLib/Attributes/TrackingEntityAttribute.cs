﻿using System;

namespace LogLib.Attributes
{
    /// <summary>
    /// Attribute for identify tracked classes and structures.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
    public class TrackingEntityAttribute : Attribute
    { }
}
