﻿using System;
using System.Reflection;
using LogInterface;

namespace LogLib
{
    /// <summary>
    /// Factory that uses reflection to create logger from specified assembly.
    /// </summary>
    public class LogFactory : ILogFactory
    {
        /// <summary>
        /// Optional report about logger creation.
        /// </summary>
        public string CreationReport { get; private set; } = "Created successfully";

        /// <summary>
        /// Logger creation options
        /// </summary>
        public LogFactoryOptions Options { get; set; }

        /// <summary>
        /// Factory method for loggers creating.
        /// </summary>
        /// <returns>ILogger instance if logger is loaded successfully, null instead.</returns>
        /// <exception cref="NullReferenceException">Thrown when Options property is null.</exception>
        public ILogger CreateLogger()
        {
            if (Options is null)
            {
                throw new NullReferenceException("Can't create " +
                                                 "logger without creation options. " +
                                                 "Please, specify Options property.");
            }

            object instance = null;

            try
            {
                Assembly assembly = Assembly.LoadFrom(Options.Assembly);
                Type type = assembly.GetType(Options.Type);

                instance = Activator.CreateInstance(type,
                    Options.Name, Options.MinLevel);

                if (instance is ILogger logger)
                {
                    logger.LoadSettings();
                    logger.Info("Logger was initialized by LogManager.");
                }
            }
            catch (Exception ex)
            {
                CreationReport = $"Logger {Options.Name}, {ex.Source} " +
                                  $"throw an exception: {ex.Message}";
            }

            return instance as ILogger;
        }
    }
}
