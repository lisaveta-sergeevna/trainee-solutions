﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using System.Text;
using LogInterface;
using LogLib.Attributes;
using LogLib.Configurations;

namespace LogLib
{
    /// <summary>
    /// Single manager for loggers maintenance.
    /// </summary>
    public  class LogManager
    {
        private readonly string _section = "loggersSection";

        private readonly List<ILogger> _loggers;

        private readonly LogFactory _factory = new();

        /// <summary>
        /// Report about loading logger from config.
        /// </summary>
        public string LoadingReport { get; private set; } = 
            "No configuration file section found.";

        /// <summary>
        /// Constructs log manager by loading loggers from config file.
        /// It is highly recommended to use constructor and LoadLoggers after it.
        /// </summary>
        public LogManager()
        {
            _loggers = new();
        }

        /// <summary>
        /// Instantiate log manager with list of loggers.
        /// </summary>
        /// <param name="loggers">List of loggers to add into log manager.</param>
        public LogManager(List<ILogger> loggers)
        {
            _loggers = new(loggers);
        }

        /// <summary>
        /// Perform loading loggers from app.config file.
        /// </summary>
        public void LoadLoggers()
        {
            if (ConfigurationManager.GetSection(_section)
                is LoggersConfig config)
            {
                int totalEntries = 0, loadedEntries = 0;
                StringBuilder report = new();

                foreach (LoggerElement loggerElement in config.Loggers)
                {
                    totalEntries++;

                    _factory.Options = new(loggerElement.Type)
                    {
                        Assembly = loggerElement.Assembly,
                        Name = loggerElement.Name,
                        MinLevel = loggerElement.MinLevel
                    };

                    ILogger logger = _factory.CreateLogger();

                    if (logger is null)
                    {
                        report.AppendLine(_factory.CreationReport);
                        continue;
                    }

                    _loggers.Add(logger);
                    loadedEntries++;
                }

                LoadingReport = $"Total sections found: {totalEntries}.\n" +
                                $"From them loaded: {loadedEntries} loggers.\n" +
                                $"{report}";
            }
        }

        /// <summary>
        /// Method used to log classes and their fields and properties, marked by attribute.
        /// </summary>
        /// <param name="instance">Instance of the object to track to.</param>
        public void Track(object instance)
        {
            if (instance is null)
            {
                throw new ArgumentNullException("Can't track null " +
                                                "reference object.");
            }

            Type type = instance.GetType();

            TrackingEntityAttribute entityAttribute = type
                .GetCustomAttribute<TrackingEntityAttribute>();

            if (entityAttribute is not null)
            {
                Dictionary<string, object> info = CollectInfo(instance, type);

                foreach (ILogger logger in _loggers)
                {
                    try
                    {
                        logger.Info("An attempt to track the object.");
                        logger.Trace(info);
                    }
                    catch (Exception ex)
                    {
                        try
                        {
                            logger.Error(ex);
                        }
                        catch { }
                    }
                }
            }
        }

        private Dictionary<string, object> CollectInfo(
            object instance, Type type)
        {
            Dictionary<string, object> markedInfo = new();

            const BindingFlags flags = BindingFlags.Public |
                                       BindingFlags.NonPublic |
                                       BindingFlags.Instance |
                                       BindingFlags.Static;

            foreach (FieldInfo fieldInfo in type.GetFields(flags))
            {
                TrackingPropertyAttribute propertyAttribute = fieldInfo
                    .GetCustomAttribute<TrackingPropertyAttribute>();

                if (propertyAttribute is not null)
                {
                    markedInfo.Add(propertyAttribute.Name ?? fieldInfo.Name,
                        fieldInfo.GetValue(instance));
                }
            }

            foreach (PropertyInfo propertyInfo in type.GetProperties(flags))
            {
                TrackingPropertyAttribute propertyAttribute = propertyInfo
                    .GetCustomAttribute<TrackingPropertyAttribute>();

                if (propertyAttribute is not null)
                {
                    markedInfo.Add(propertyAttribute.Name ?? propertyInfo.Name,
                        propertyInfo.GetValue(instance));
                }
            }

            return markedInfo;
        }
    }
}
