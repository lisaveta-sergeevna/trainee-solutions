﻿using System;
using System.Configuration;
using LogInterface;

namespace LogLib.Configurations
{
    /// <summary>
    /// Listener section element.
    /// </summary>
    public class LoggerElement : ConfigurationElement
    {
        /// <summary>
        /// Listener name, optional.
        /// </summary>
        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get => base["name"] as string;
            set => base["name"] = value;
        }

        /// <summary>
        /// Type of the listener in string format.
        /// </summary>
        [ConfigurationProperty("type", IsRequired = true)]
        public string Type
        {
            get => base["type"] as string;
            set => base["type"] = value;
        }

        /// <summary>
        /// Assembly, where type located.
        /// </summary>
        [ConfigurationProperty("assembly", IsRequired = true)]
        public string Assembly
        {
            get => base["assembly"] as string;
            set => base["assembly"] = value;
        }

        /// <summary>
        /// Minimum logging level.
        /// </summary>
        [ConfigurationProperty("minLevel", IsRequired = true)]
        public LogLevel MinLevel
        {
            get => (LogLevel)base["minLevel"];
            set
            {
                if (!Enum.IsDefined(typeof(LogLevel), value))
                {
                    throw new ArgumentException("No such enum constant" +
                                                " defined in LogLevel.");
                }

                base["minLevel"] = value;
            }
        }

        /// <summary>
        /// Default empty constructor.
        /// </summary>
        public LoggerElement() { }

        /// <summary>
        /// Constructor with logger fields.
        /// </summary>
        /// <param name="name">Name of the listener.</param>
        /// <param name="type">Type of the logger to load.</param>
        /// <param name="assembly">Assembly where logger type locates.</param>
        /// <param name="minLevel">Minimum logging level.</param>
        public LoggerElement(string name, string type, string assembly, LogLevel minLevel)
        {
            Name = name;
            Type = type;
            Assembly = assembly;
            MinLevel = minLevel;
        }
    }
}
