﻿using System.Configuration;

namespace LogLib.Configurations
{
    /// <summary>
    /// Collection of listener elements.
    /// </summary>
    public class LoggersCollection : ConfigurationElementCollection
    {
        /// <summary>
        /// Default empty constructor.
        /// </summary>
        public LoggersCollection() { }

        /// <summary>
        /// Indexer of the collection.
        /// </summary>
        /// <param name="i">Index of the element in the collection.</param>
        /// <returns>LoggerElement at the i position.</returns>
        public LoggerElement this[int i]
        {
            get => BaseGet(i) as LoggerElement;
            set => BaseAdd(value);
        }

        /// <summary>
        /// Add new listener into collection.
        /// </summary>
        /// <param name="element">Element to insert.</param>
        public void Add(LoggerElement element) 
            => BaseAdd(element);

        /// <summary>
        /// Empty current collection.
        /// </summary>
        public void Clear() => BaseClear();

        /// <summary>
        /// Remove specified element from the collection.
        /// </summary>
        /// <param name="element">Element to remove to.</param>
        public void Remove(LoggerElement element) 
            => BaseRemove(element);

        /// <summary>
        /// Remove element at the specified position in the collection.
        /// </summary>
        /// <param name="index">Index of the element in the collection to remove.</param>
        public void RemoveAt(int index) 
            => BaseRemoveAt(index);

        /// <summary>
        /// Creates an instance of Listener element.
        /// </summary>
        /// <returns>Empty instance of LoggerElement.</returns>
        protected override ConfigurationElement CreateNewElement()
            => new LoggerElement();

        /// <summary>
        /// Specifies key element of Listener.
        /// </summary>
        /// <param name="element">Element to return key to.</param>
        /// <returns>Name of current Listener instance as key.</returns>
        protected override object GetElementKey(ConfigurationElement element) 
            => (element as LoggerElement)?.Name;
    }
}
