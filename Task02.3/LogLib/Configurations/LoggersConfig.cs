﻿using System.Configuration;

namespace LogLib.Configurations
{
    /// <summary>
    /// Section of listeners to load into LogManager.
    /// </summary>
    public class LoggersConfig : ConfigurationSection
    {
        /// <summary>
        /// Collection of listeners in the section.
        /// </summary>
        [ConfigurationProperty("loggers", IsRequired = false)]
        [ConfigurationCollection(typeof(LoggersCollection))]
        public LoggersCollection Loggers
            => base["loggers"] as LoggersCollection;
    }
}
