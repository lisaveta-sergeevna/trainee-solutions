﻿using System;
using System.IO;
using System.Numerics;
using LogLib.Attributes;

namespace LogLib
{
    internal class Program
    {
        [TrackingEntity]
        private class TrackedClass
        {
            [TrackingProperty("configuration")]
            private int _config = 10;

            private string _nonTrackedString = "no tracking";

            [TrackingProperty]
            public Type type = typeof(double);

            [TrackingProperty("big integer")]
            private BigInteger BI { get; set; } = BigInteger.MinusOne;

            [TrackingProperty]
            public double SomeDouble { get; set; } = 13.01;
        }

        internal static void PrintFileContents(string file)
        {
            Console.WriteLine($"Contents of file \"{file}\":\n");

            using StreamReader reader = new(file);

            string line;

            while ((line = reader.ReadLine()) is not null)
            {
                Console.WriteLine(line);
            }

            Console.WriteLine();
        }

        internal static void Main(string[] args)
        {
            LogManager manager = new();

            try
            {
                manager.LoadLoggers();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return;
            }

            Console.WriteLine(manager.LoadingReport);

            TrackedClass tracked = new();

            try
            {
                manager.Track(tracked);

                foreach (string file in Directory.EnumerateFiles(
                    Directory.GetCurrentDirectory(),
                    $"{DateTime.Now:yy-MM-dd}.*"))
                {
                    PrintFileContents(file);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
