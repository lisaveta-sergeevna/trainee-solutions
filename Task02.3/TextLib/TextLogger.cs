﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using LogInterface;

namespace TextLib
{
    /// <summary>
    /// Write logging info into text file.
    /// </summary>
    public class TextLogger : ILogger
    {
        private readonly string _logFileKey = "logFile";

        /// <summary>
        /// Logger name.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Text file to write log info into.
        /// </summary>
        public string LogFile { get; private set; } = "log.txt";

        /// <summary>
        /// Minimum enabled logging level.
        /// </summary>
        public LogLevel MinLevel { get; }

        /// <summary>
        /// Logger constructor.
        /// </summary>
        /// <param name="name">Logger name.</param>
        /// <param name="minLevel">Minimum available level of logs.</param>

        public TextLogger(string name, LogLevel minLevel)
        {
            Name = name;
            MinLevel = minLevel;
        }

        /// <summary>
        /// Load logger settings from app.config. Recommended to use after .ctor in try-catch.
        /// </summary>
        public void LoadSettings()
        {
            AppSettingsSection appSettings = ConfigurationManager
                .OpenExeConfiguration(GetType().Namespace + ".dll").AppSettings;

            if (appSettings is not null && appSettings.Settings
                .AllKeys.Contains(_logFileKey))
            {
                string format = appSettings.Settings[_logFileKey].Value;
                LogFile = string.Format(format, DateTime.Now);
            }
        }

        /// <summary>
        /// Log message on specified level.
        /// </summary>
        /// <param name="level">Level to log on.</param>
        /// <param name="message">Message to log to.</param>
        /// <exception cref="ArgumentException">Thrown when level is not enabled, 
        /// LogFile path contains an empty string or the name of system device.</exception>
        public void Log(LogLevel level, string message)
        {
            if (MinLevel <= level)
            {
                string entry = $"{DateTime.Now:R} | {Name ?? "Logger"} | {level} | {message}";

                using StreamWriter writer = new(LogFile, true);
                writer.WriteLine(entry);
            }
            else
            {
                throw new ArgumentException($"Level {level} is not enabled for this logger.");
            }
        }
    }
}
