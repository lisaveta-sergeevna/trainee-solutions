using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using LogInterface;
using LogLib.Attributes;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LogLib.Tests
{
    [TestClass]
    public class LogManagerTests
    {
        [TrackingEntity]
        private class TrackedClass
        {
            [TrackingProperty("configuration")] 
            private int _config = 10;

            private string _nonTrackedString = "no tracking";

            [TrackingProperty]
            public Type type = typeof(double);

            [TrackingProperty("big integer")]
            private BigInteger BI { get; set; } = BigInteger.MinusOne;

            [TrackingProperty]
            public double SomeDouble { get; set; } = 13.01;
        }

        private readonly string _trackedFields = "configuration = 10\n" +
                                                 "type = System.Double\n" +
                                                 "big integer = -1\n" +
                                                 "SomeDouble = 13.01\n";

        private class LoggerMock : ILogger
        {
            public string Name => "LoggerMock";

            public string File => "log.txt";

            public LogLevel MinLevel { get; }

            public string ErrorLog { get; set; }

            public string InfoLog { get; set; }

            public string TraceLog { get; set; }

            public void Error(Exception exception)
            {
                if (MinLevel <= LogLevel.Error)
                {
                    ErrorLog = exception.Message;
                }
            } 

            public void Info(string message)
            {
                if (MinLevel <= LogLevel.Info)
                {
                    InfoLog = message;
                }
            }

            public void LoadSettings()
            { }

            public void Log(LogLevel level, string message)
            { }

            public void Trace(Dictionary<string, object> parameters)
            {
                if (MinLevel <= LogLevel.Trace)
                {
                    StringBuilder message = new();

                    foreach (KeyValuePair<string, object> pair in parameters)
                    {
                        message.Append($"{pair.Key} = {pair.Value}\n");
                    }

                    TraceLog = message.ToString();
                }
                else
                {
                    throw new ArgumentException("Trace level is not allowed.");
                }
            }

            public LoggerMock(LogLevel minLevel) => MinLevel = minLevel;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Track_WhenInstanceIsNull_ThrowException()
        {
            LogManager manager = new();
            manager.Track(null);
        }

        [TestMethod]
        public void Track_WhenInstanceIsNotTrackEntity_IgnoreInstance()
        {
            LoggerMock logger = new(LogLevel.Trace);
            List<ILogger> loggersMock = new() { logger };
            LogManager manager = new(loggersMock);

            manager.Track(logger);

            Assert.IsNull(logger.TraceLog);
            Assert.IsNull(logger.ErrorLog);
            Assert.IsNull(logger.InfoLog);
        }

        [TestMethod]
        public void Track_WhenInstanceIsTrackedEntity_TraceTrackProperties()
        {
            LoggerMock logger = new(LogLevel.Trace);
            List<ILogger> loggersMock = new() { logger };
            LogManager manager = new(loggersMock);

            manager.Track(new TrackedClass());

            CollectionAssert.AreEquivalent(_trackedFields.Split('\n'),
                logger.TraceLog.Split('\n'));
        }

        [TestMethod]
        public void Track_WhenLoggerDoNotAllowTrace_WriteToErrorLog()
        {
            LoggerMock logger = new(LogLevel.Info);
            List<ILogger> loggersMock = new() { logger };
            LogManager manager = new(loggersMock);

            manager.Track(new TrackedClass());

            Assert.AreEqual("Trace level is not allowed.", logger.ErrorLog);
        }
    }
}
