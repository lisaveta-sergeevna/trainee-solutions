﻿using System;
using LogInterface;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WordLib;

namespace LogLib.Tests
{
    [TestClass]
    public class WordLoggerTests
    {
        [DataTestMethod]
        [DataRow(LogLevel.Info, LogLevel.Trace)]
        [DataRow(LogLevel.Error, LogLevel.Info)]
        [DataRow(LogLevel.Error, LogLevel.Trace)]
        [ExpectedException(typeof(ArgumentException))]
        public void Log_WhenLevelIsNotEnabled_ThrowException(
            LogLevel minLevel, LogLevel currentLevel)
        {
            WordLogger logger = new("name", minLevel);
            logger.Log(currentLevel, "some message");
        }
    }
}
