﻿using System;
using System.Text;

namespace TrainingMaterials
{
    /// <summary>
    /// Training lesson with materials collection.
    /// </summary>
    public class Lesson : Entity, IVersionable, ICloneable
    {
        private Material[] _materials;

        private byte[] _version;

        /// <summary>
        /// Type of the lesson, characterised by it material types.
        /// </summary>
        public LessonType Type { get; private set; }

        /// <summary>
        /// Constructor to set the description and array of materials.
        /// </summary>
        /// <param name="description">Optional entity description string.</param>
        /// <param name="materials">Array with materials to copy data from.</param>
        public Lesson(string description = null, 
            params Material[] materials) : base(description)
        {
            if (materials is not null)
            {
                _materials = new Material[materials.Length];
                Array.Copy(materials, _materials, materials.Length);
            }

            _version = new byte[IVersionable.bytesCount];
            Type = LessonType.Undefined;

            foreach (Material material in _materials)
            {
                if (material is TextMaterial)
                {
                    Type = LessonType.TextLesson;
                }

                if (material is VideoMaterial)
                {
                    Type = LessonType.VideoLesson;
                    break;
                }
            }
        }

        /// <summary>
        /// Method to add some new materials to lesson.
        /// </summary>
        /// <param name="material">The copy of this material will be added to lesson.</param>
        /// <exception cref="ArgumentNullException">Thrown when material param is null.</exception>
        public void Add(Material material)
        {
            if (material is null)
            {
                throw new ArgumentNullException("Can't add null reference value" +
                    " into Lesson materials.");
            }

            Array.Resize(ref _materials, _materials.Length + 1);
            _materials[_materials.Length - 1] = material.Clone();

            if (Type == LessonType.Undefined && material is TextMaterial)
            {
                Type = LessonType.TextLesson;
                return;
            }

            if (Type != LessonType.VideoLesson && material is VideoMaterial)
            {
                Type = LessonType.VideoLesson;
            }
        }

        /// <summary>
        /// ToString overring for output.
        /// </summary>
        /// <returns>String representation of lesson properties and materials array.</returns>
        public override string ToString()
        {
            StringBuilder lesson = new(base.ToString() + 
                $"Lesson type: {Type}\n" +
                $"Materials:\n\n");

            foreach (Material material in _materials)
            {
                lesson.Append($"{material}\n");
            }

            return lesson.ToString();
        }

        /// <summary>
        /// IVersionable method to get version array copy.
        /// </summary>
        /// <returns>Copy of 8 byte version array.</returns>
        public byte[] GetVersion()
        {
            return (this as IVersionable).CopyVersion(_version);
        }

        /// <summary>
        /// IVersionable method to set new version.
        /// </summary>
        /// <param name="version">8 byte array to be copied into version array.</param>
        public void UpdateVersion(byte[] version)
        {
            Array.Copy(version, _version, version.Length);
        }

        /// <summary>
        /// ICloneable method, deep lesson cloning (with all materials cloning).
        /// </summary>
        /// <returns>New lesson object with the same materials, version and properties.</returns>
        public object Clone()
        {
            Material[] materials = new Material[_materials.Length];

            for (int i = 0; i < _materials.Length; i++)
            {
                materials[i] = _materials[i].Clone();
            }

            Lesson newLesson = new Lesson(Description, materials) { GUID = GUID };
            newLesson.UpdateVersion(_version);

            return newLesson;
        }
    }
}
