﻿using System;

namespace TrainingMaterials
{
    /// <summary>
    /// Class for collection Materials reprosentation and convenient cloning.
    /// </summary>
    public abstract class Material : Entity
    { 
        /// <summary>
        /// Constructor for accurate base constructor calling.
        /// </summary>
        /// <param name="description">Optional entity description string.</param>
        public Material(string description = null) : base(description)
        { }

        /// <summary>
        /// Abstract method for uniform materials cloning.
        /// </summary>
        /// <returns>Material deep copy.</returns>
        public abstract Material Clone();
    }
}
