﻿using System;

namespace TrainingMaterials
{
    /// <summary>
    /// Video material versionable representation class.
    /// </summary>
    public class VideoMaterial : Material, IVersionable
    {
        private byte[] _version;

        /// <summary>
        /// Link to material file, not null or empty.
        /// </summary>
        public string ContentURI { get; }

        /// <summary>
        /// Optional link to content image.
        /// </summary>
        public string ImageURI { get; }

        /// <summary>
        /// Format of the stored video.
        /// </summary>
        public VideoFormat Format { get; }

        /// <summary>
        /// Constructor with arguments check.
        /// </summary>
        /// <param name="contentURI">Required content link string, not null or empty.</param>
        /// <param name="imageURI">Optional image link, can be null or empty.</param>
        /// <param name="format">Value from VideoFormat enum.</param>
        /// <param name="description">Optional entity description.</param>
        /// <exception cref="ArgumentException">Thrown when contentURI is null or empty, 
        /// or then format value is not defined in VideoFormat enum.</exception>
        public VideoMaterial(string contentURI, string imageURI, 
            VideoFormat format, string description = null) 
            : base(description)
        {
            if (string.IsNullOrEmpty(contentURI))
            {
                throw new ArgumentException("Content URI string can't be null or empty.");
            }

            if (!Enum.IsDefined(typeof(VideoFormat), format))
            {
                throw new ArgumentException("Invalid video format enum value.");
            }

            _version = new byte[IVersionable.bytesCount];

            ContentURI = contentURI;
            ImageURI = imageURI;
            Format = format;
        }

        /// <summary>
        /// ToString overrring for output.
        /// </summary>
        /// <returns>String representation of VideoMaterials properties.</returns>
        public override string ToString()
        {
            return base.ToString() +
                $"Type: {typeof(VideoMaterial)}\n" +
                $"Content URI: {ContentURI}\n" +
                $"Image URI: {ImageURI ?? "-"}\n" +
                $"Format: {Format}\n";
        }

        /// <summary>
        /// IVersionable method to get version array.
        /// </summary>
        /// <returns>The copy of versions array.</returns>
        public byte[] GetVersion()
        {
            return (this as IVersionable).CopyVersion(_version);
        }

        /// <summary>
        /// IVersionable mathod to update version array.
        /// </summary>
        /// <param name="version">New array to copy into version.</param>
        public void UpdateVersion(byte[] version)
        {
            Array.Copy(version, _version, version.Length);
        }

        /// <summary>
        /// Cloning method for deep VideoMaterial cloning.
        /// </summary>
        /// <returns>New VideoMaterial object with the same property values.</returns>
        public override Material Clone()
        {
            return new VideoMaterial(ContentURI, ImageURI, Format, Description) { GUID = GUID };
        }
    }
}
