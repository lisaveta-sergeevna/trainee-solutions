﻿using System;

namespace TrainingMaterials
{
    /// <summary>
    /// Text material representation class.
    /// </summary>
    public class TextMaterial : Material
    {
        private static readonly int _maxTextLength = 10_000;

        /// <summary>
        /// Text content of the material.
        /// </summary>
        public string Text { get; }

        /// <summary>
        /// Text material constructor. Check for the valid text parameter.
        /// </summary>
        /// <param name="text">Required text of material.</param>
        /// <param name="description">Optional entity description.</param>
        /// <exception cref="ArgumentException">Thrown when text param string is null or empty 
        /// or text length is more then 10_000 chars.</exception>
        public TextMaterial(string text,string description = null) 
            : base(description)
        {
            if (string.IsNullOrEmpty(text))
            {
                throw new ArgumentException("Text string can't be null or empty.");
            }

            if (text.Length > _maxTextLength)
            {
                throw new ArgumentException($"Text string can't be longer " +
                    $"then {_maxTextLength} characters.");
            }

            Text = text;
        }

        /// <summary>
        /// Tostring overriding.
        /// </summary>
        /// <returns>String representation of GUID, Description and Text of TextMaterial.</returns>
        public override string ToString()
        {
            return base.ToString() +
                $"Type: {typeof(TextMaterial)}\n" +
                $"Text: {Text}\n";
        }

        /// <summary>
        /// TextMaterial deep cloning method.
        /// </summary>
        /// <returns>New TextMaterial with the same properties.</returns>
        public override Material Clone()
        {
            return new TextMaterial(Text, Description) { GUID = GUID };
        }
    }
}
