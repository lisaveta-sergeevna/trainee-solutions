﻿using System;

namespace TrainingMaterials
{
    /// <summary>
    /// Web material represents a link to html page, image or video on the web.
    /// </summary>
    public class WebMaterial : Material
    {
        /// <summary>
        /// Web link to content, not null or empty.
        /// </summary>
        public string ContentURI { get; }

        /// <summary>
        /// Type of web content, stored via the link.
        /// </summary>
        public WebFormat Format { get; }

        /// <summary>
        /// Constructor with parameters check.
        /// </summary>
        /// <param name="contentURI">Required content link on the web.</param>
        /// <param name="format">Enum value that represents the type of the content.</param>
        /// <param name="description">Optional entity description string.</param>
        /// <exception cref="ArgumentException">Thrown when content link string is null or empty 
        /// or when format enum value is not defined.</exception>
        public WebMaterial(string contentURI, WebFormat format, 
            string description = null) : base(description)
        {
            if (string.IsNullOrEmpty(contentURI))
            {
                throw new ArgumentException("Content URI string can't be null or empty.");
            }

            if (!Enum.IsDefined(typeof(WebFormat), format))
            {
                throw new ArgumentException("Invalid web format enum value.");
            }

            ContentURI = contentURI;
            Format = format;
        }

        /// <summary>
        /// ToString for output.
        /// </summary>
        /// <returns>String representation of Web material and it' properties.</returns>
        public override string ToString()
        {
            return base.ToString() +
                $"Type: {typeof(WebMaterial)}\n" +
                $"Content URI: {ContentURI}\n" +
                $"Format: {Format}\n";
        }

        /// <summary>
        /// Deep web material cloning method.
        /// </summary>
        /// <returns>New web material object whith same properties.</returns>
        public override Material Clone()
        {
            return new WebMaterial(ContentURI, Format, Description) { GUID = GUID };
        }
    }
}
