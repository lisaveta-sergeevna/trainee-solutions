﻿using System;

namespace TrainingMaterials
{
    public static class EntityExtensions
    {
        public static void GenerateNewGuid(this Entity entity)
        {
            if (entity is null)
            {
                throw new NullReferenceException("Can't call an extension method " +
                    "for null-value reference.");
            }

            entity.GUID = Guid.NewGuid();
        }
    }
}
