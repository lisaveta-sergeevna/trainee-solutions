﻿using System;

namespace TrainingMaterials
{
    /// <summary>
    /// Versionable interface for 8 byte version array.
    /// </summary>
    public interface IVersionable
    {
        /// <summary>
        /// Version size in bytes.
        /// </summary>
        static int bytesCount = 8;

        /// <summary>
        /// Standard method to copy version array from the source.
        /// </summary>
        /// <param name="version">Sourse array, data to be copied.</param>
        /// <returns>New array with the same data.</returns>
        byte[] CopyVersion(byte[] version)
        {
            byte[] copy = new byte[bytesCount];
            Array.Copy(version, copy, bytesCount);

            return copy;
        }

        byte[] GetVersion();

        void UpdateVersion(byte[] version);
    }
}
