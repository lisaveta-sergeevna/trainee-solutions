﻿namespace TrainingMaterials
{
    public enum WebFormat
    {
        Unknown,
        Html,
        Image,
        Audio,
        Video
    }
}
