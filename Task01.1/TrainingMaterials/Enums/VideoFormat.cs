﻿namespace TrainingMaterials
{
    public enum VideoFormat
    {
        Unknown,
        Avi,
        Mp4,
        Flv
    }
}
