﻿namespace TrainingMaterials
{
    public enum LessonType
    {
        Undefined,
        TextLesson,
        VideoLesson
    }
}
