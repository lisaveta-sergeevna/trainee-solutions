﻿using System;

namespace TrainingMaterials
{
    /// <summary>
    /// Base entity with GUID and Description.
    /// </summary>
    public abstract class Entity
    {
        private static readonly int _maxDescriptionLength = 256;

        /// <summary>
        /// Statisticaly unique indentifier of the Entity.
        /// </summary>
        public Guid GUID { get; set; }

        /// <summary>
        /// Addtitional text description of the entity.
        /// </summary>
        public string Description { get; }

        /// <summary>
        /// Entity constructor with description string.
        /// </summary>
        /// <param name="description">Optional Entity description.</param>
        /// <exception cref="ArgumentException">Thrown when description is longer then maximum.</exception>
        public Entity(string description = null)
        {
            if (!string.IsNullOrEmpty(description) && 
                description.Length > _maxDescriptionLength)
            {
                throw new ArgumentException($"Entity description string " +
                    $"can't be longer then {_maxDescriptionLength} characters.");
            }

            GUID = Guid.NewGuid();
            Description = description;
        }

        /// <summary>
        /// Standard ToString overloading for output.
        /// </summary>
        /// <returns>Entity GUID and Description in string format.</returns>
        public override string ToString()
        {
            return $"GUID: {GUID}\n" +
                $"Description: {Description ?? "-"}\n";
        }

        /// <summary>
        /// Equals overriding, compares Entities by GUID.
        /// </summary>
        /// <param name="obj">Other entity to compare with.</param>
        /// <returns>True - if other object is an Entity with equal GUID, else - false.</returns>
        /// <exception cref="ArgumentException">If obj parameter is null.</exception>
        /// <exception cref="InvalidCastException">If obj parameter can't be represented as Entity.</exception>
        public override bool Equals(object obj)
        {
            if (obj is null)
            {
                throw new ArgumentNullException("Can't compare the equality " +
                    "with null-value reference.");
            }

            if (obj is Entity entity)
            {
                return GUID.Equals(entity.GUID);
            }

            throw new InvalidCastException("Can't compare the equality " +
                "between different types.");
        }

        /// <summary>
        /// Hashcode of entity, recounted by GUID.
        /// </summary>
        /// <returns>Hashcode of entity GUID.</returns>
        public override int GetHashCode()
        {
            return GUID.GetHashCode();
        }
    }
}
