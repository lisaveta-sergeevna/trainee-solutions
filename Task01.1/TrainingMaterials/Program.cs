﻿using System;

namespace TrainingMaterials
{
    internal class Program
    {
        internal static void PrintVersionArray(byte[] version)
        {
            foreach (byte b in version)
            {
                Console.Write($"{b} ");
            }

            Console.WriteLine();
        }

        internal static void Main(string[] args)
        {
            Material[] materials;

            try
            {
                materials = new Material[]
                {
                    new TextMaterial("Some text about C# theory."),
                    new WebMaterial("sharepoint.com/webs/web1", WebFormat.Html)
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }

            Lesson lesson, clone;

            try
            {
                lesson = new(materials: materials);
                clone = lesson.Clone() as Lesson;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }

            Console.WriteLine(lesson);
            Console.WriteLine("Lesson version: ");

            PrintVersionArray(lesson.GetVersion());

            if (lesson.Equals(clone))
            {
                Console.WriteLine("\nEntities have the same GUID:");
                Console.WriteLine($"{lesson.GUID} = {clone.GUID}");
                clone.GenerateNewGuid();
            }

            lesson.Add(new VideoMaterial("sharepoint.com/videos/video1",
                        "sharepoint.com/images/image1", VideoFormat.Avi));
            lesson.UpdateVersion(BitConverter.GetBytes(1000));

            Console.WriteLine($"Updated lesson version: ");
            PrintVersionArray(lesson.GetVersion());

            Console.WriteLine($"Updated lesson:\n\n{lesson}\n");
            Console.WriteLine($"Lesson clone:\n\n{clone}");
        }
    }
}
