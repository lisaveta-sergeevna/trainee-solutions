﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ElectronicCatalog;
using System;

namespace ElectronicCatalogTest
{
    [TestClass]
    public class IsbnTests
    {
        [DataTestMethod]
        [DataRow(null, DisplayName = "Null value")]
        [DataRow("", DisplayName = "Empty value")]
        [ExpectedException(typeof(ArgumentException))]
        public void Constructor_WhenIsbnIsNullOrEmpty_ThrowException(string line)
        {
            Isbn isbn = new(line);
        }

        [DataTestMethod]
        [DataRow("12b4567a90123", DisplayName = "String with letters")]
        [DataRow("123-45-679-9012-3", DisplayName = "String with wrong slashes position")]
        [ExpectedException(typeof(FormatException))]
        public void Constructor_WhenIsbnHasWrongFormat_ThrowException(string line)
        {
            Isbn isbn = new(line);
        }

        [DataTestMethod]
        [DataRow("123-4-56-789012-3", DisplayName = "Isbn with slashes")]
        [DataRow("1234567890123", DisplayName = "Isbn without slashes")]
        public void Equals_WhenSameIsbnPassed_ReturnTrue(string line)
        {
            Isbn isbn = new(line);

            Assert.IsTrue(isbn.Equals(line));
        }

        [DataTestMethod]
        [DataRow("123-4-56-789012-3", "123-4-56-789012-4", 
            DisplayName = "Different valid Isbns")]
        public void Equals_WhenDifferentIsbnPassed_ReturnFalse(string actual, string line)
        {
            Isbn isbn = new(actual);

            Assert.IsFalse(isbn.Equals(line));
        }

        [TestMethod]
        public void Equals_WhenObjIsSameIsbn_ReturnTrue()
        {
            string line = "123-4-56-789012-3";
            Isbn isbn1 = new(line);
            Isbn isbn2 = new(line);

            Assert.IsTrue(isbn1.Equals((object)isbn2));
        }

        [TestMethod]
        public void GetHashCode_WithSameStringHashCode_AreEqual()
        {
            string line = "1234567890123";
            Isbn isbn = new(line);

            Assert.AreEqual(line.GetHashCode(), isbn.GetHashCode());
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidCastException))]
        public void Equals_WhenObjIsNotIsbn_ThrowException()
        {
            string line = "123-4-56-789012-3";
            Isbn isbn = new(line);
            Author author = new("Ivan", "Ivanov");

            isbn.Equals(author);
        }

        [TestMethod]
        public void ToString_WhenIsbnWithoutSlashes_AreEqualWithInitialString()
        {
            string line = "1234567890123";
            Isbn isbn = new(line);

            Assert.AreEqual(line, isbn.ToString());
        }
    }
}
