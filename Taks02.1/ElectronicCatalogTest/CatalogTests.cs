﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ElectronicCatalog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ElectronicCatalogTest
{
    [TestClass]
    public class CatalogTests
    {
        private static readonly List<Book> _books = new()
        {
            new("1111111111111", "bbbbbb"),
            new("1234567890123", "AAAA", DateTime.Now, 
                new("Ivan", "Ivanov"), 
                new("Petr", "Petrov")),
            new("222-2-22-222222-2", "Eeeeee", DateTime.MinValue,
                new Author("petr", "PETROV")),
            new("333-3-33-333333-3", "ccccC", DateTime.Today)
        };

        private static readonly Catalog _catalog = new(_books);

        [DataTestMethod]
        [DataRow("1234567890123", "123-4-56-789012-3")]
        [DataRow("123-4-56-789012-3", "1234567890123")]
        public void Indexer_WhenUsedDifferentIsbnFormats_ReturnEqualBook(string isbn, string otherIsbn)
        {
            Catalog catalog = new();
            Book result = new(isbn, "Title");

            catalog.Add(isbn, result);
            Book actual = catalog[otherIsbn];

            Assert.AreEqual(result, actual);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Constructor_WhenBooksIsNull_ThrowException()
        {
            Catalog catalog = new(null);
        }

        [TestMethod]
        public void GetEnumerator_ReturnSortedCollection()
        {
            List<Book> result = new(_books);
            List<Book> actual = new(_catalog);

            result.Sort((book, other) 
                => string.CompareOrdinal(book.Title.ToLower(), 
                    other.Title.ToLower()));

            CollectionAssert.AreEqual(result, actual);
        }

        [TestMethod]
        public void BooksByAuthor_ReturnAuthorBooks()
        {
            List<Book> result = new();
            Author author = new("ivan", "IVANOV");
            List<Book> actual = new(_catalog.BooksByAuthor(author.Name, author.Lastname));

            foreach (Book book in _books)
            {
                if (book.Authors.Contains(author))
                {
                    result.Add(book);
                }
            }

            CollectionAssert.AreEquivalent(result, actual);
        }

        [TestMethod]
        public void BooksSortedByPublicationDate_ReturnBooksSortedByDescendingDate()
        {
            List<Book> result = new();
            List<Book> actual = new(_catalog.BooksSortedByPublicationDate());

            foreach (Book book in _books)
            {
                if (book.PublicationDate.HasValue)
                {
                    result.Add(book);
                }
            }

            result.Sort((book, other) => -DateTime.Compare(book.PublicationDate.Value, 
                other.PublicationDate.Value));

            CollectionAssert.AreEqual(result, actual);
        }

        [TestMethod]
        public void CountBooksByAuthors_ReturnBooksCountForEachAuthor()
        {
            List<(Author author, int booksCount)> result = new();
            List<(Author, int)> actual = new(_catalog.CountBooksByAuthors());

            foreach (Book book in _books)
            {
                foreach (Author bookAuthor in book.Authors)
                {
                    int ind = result.FindIndex(pair 
                        => pair.author.Equals(bookAuthor));

                    if (ind == -1)
                    {
                        result.Add((bookAuthor, 1));
                    }
                    else
                    {
                        result[ind] = (bookAuthor, result[ind].booksCount + 1);
                    }
                }
            }

            CollectionAssert.AreEquivalent(result, actual);
        }
    }
}
