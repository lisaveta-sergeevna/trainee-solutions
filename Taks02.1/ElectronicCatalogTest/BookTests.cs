﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ElectronicCatalog;
using System;

namespace ElectronicCatalogTest
{
    [TestClass]
    public class BookTests
    {
        [TestMethod]
        public void Constructor_WithValidAuthors_CopyEquivalentArrayToProperty()
        {
            string isbn = "1234567890123";
            string title = "Valid title";
            Author[] authors =
            {
                new("AuthorName1", "AuthorLastname1"),
                new("AuthorName2", "AuthorLastname2")
            };
            Book book = new(isbn, title, null, authors);
            Author[] bookAuthors = book.Authors;

            CollectionAssert.AreEquivalent(authors, bookAuthors);
        }

        [DataTestMethod]
        [DataRow("1234567890123", null, DisplayName = "Title is null")]
        [DataRow("1234567890123", "", DisplayName = "Title is empty")]
        [DataRow("1234567890123", "One morning, when Gregor Samsa woke from " +
                                  "troubled dreams, he found himself transformed in his bed into a horrible " +
                                  "vermin. He lay on his armour-like back, and if he lifted his head a little " +
                                  "he could see his brown belly, slightly domed and divided by arches into stiff " +
                                  "sections. The bedding was hardly able to cover it and seemed ready to slide off" +
                                  " any moment. His many legs, pitifully thin compared with the size of the rest of" +
                                  " him, waved about helplessly as he looked. \"What's happened to me?\" " +
                                  "he thought. It wasn't a dream.His room, a proper human room although a little " +
                                  "too small, lay peacefully between its four familiar walls.A collection of " +
                                  "textile samples lay spread out on the table - Samsa was a travelling salesman " +
                                  "- and above it there hung a picture that he had recently cut out of an " +
                                  "illustrated magazine and housed in a nice, gilded frame.It showed a lady " +
                                  "fitted out with a fur hat and fur boa who sat upright, raising a heavy " +
                                  "fur muff that covered the whole of her lower arm towards the viewer. " +
                                  "Gregor then turned to look out the window at the dull weather. " +
                                  "Drops of rain could be hea", DisplayName = "Title > 1000")]
        [DataRow("1234567890123", "One morning, when Gregor Samsa woke from " +
                                  "troubled dreams, he found himself transformed in his bed into a horrible " +
                                  "vermin. He lay on his armour-like back, and if he lifted his head a little " +
                                  "he could see his brown belly, slightly domed and divided by arches into stiff " +
                                  "sections. The bedding was hardly able to cover it and seemed ready to slide off" +
                                  " any moment. His many legs, pitifully thin compared with the size of the rest of" +
                                  " him, waved about helplessly as he looked. \"What's happened to me?\" he thought." +
                                  " It wasn't a dream.His room, a proper human room although a little too small, lay" +
                                  " peacefully between its four familiar walls.A collection of textile samples lay spread" +
                                  " out on the table - Samsa was a travelling salesman - and above it there hung a picture" +
                                  " that he had recently cut out of an illustrated magazine and housed in a nice, gilded" +
                                  " frame.It showed a lady fitted out with a fur hat and fur boa who sat upright, raising" +
                                  " a heavy fur muff that covered the whole of her lower arm towards the v", 
                                    DisplayName = "Title = 1001")]
        [ExpectedException(typeof(ArgumentException))]
        public void Constructor_WithInvalidParameters_ThrowException(string isbn, string title)
        {
            Book book = new(isbn, title);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Equals_WhenOtherBookIsNull_ThrowException()
        {
            string isbn = "1234567890123";
            string title = "Valid title";
            Book book = new(isbn, title);

            book.Equals(null);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidCastException))]
        public void Equals_WhenObjIsNotBook_ThrowException()
        {
            string isbn = "1234567890123";
            string title = "Valid title";
            Book book = new(isbn, title);

            book.Equals(title);
        }

        [DataTestMethod]
        [DataRow("1234567890123", "Title1", "1234567890123", "Title2")]
        [DataRow("123-4-56-789012-3", "Title1", "1234567890123", "Title2")]
        [DataRow("1234567890123", "Title1", "123-4-56-789012-3", "Title2")]
        public void Equals_WithSameBookIsbn_ReturnTrue(string isbn, string title, 
            string otherIsbn, string otherTitle)
        {
            Book book1 = new(isbn, title);
            Book book2 = new(otherIsbn, otherTitle);

            Assert.IsTrue(book1.Equals(book2));
        }

        [TestMethod]
        public void Equals_WithDifferentBookIsbn_ReturnFalse()
        {
            string isbn1 = "1234567890123";
            string isbn2 = "1234567890125";
            string title = "Valid title";
            Book book1 = new(isbn1, title);
            Book book2 = new(isbn2, title);

            Assert.IsFalse(book1.Equals(book2));
        }

        [TestMethod]
        public void Equals_WhenObjIsSameBook_ReturnTrue()
        {
            string isbn1 = "1234567890123";
            string isbn2 = "1234567890123";
            string title = "Valid title";
            Book book1 = new(isbn1, title);
            Book book2 = new(isbn2, title);

            Assert.IsTrue(book1.Equals((object)book2));
        }
    }
}
