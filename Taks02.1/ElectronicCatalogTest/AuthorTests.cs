using Microsoft.VisualStudio.TestTools.UnitTesting;
using ElectronicCatalog;
using System;

namespace ElectronicCatalogTest
{
    [TestClass]
    public class AuthorTests
    {
        [TestMethod]
        public void Equals_WithSameAuthor_IsTrue()
        {
            string name = "Ivan";
            string lastname = "Ivanov";
            Author author1 = new(name, lastname);
            Author author2 = new(name, lastname);

            Assert.IsTrue(author1.Equals(author2));
        }

        [TestMethod]
        public void Equals_WithDifferentAuthor_IsFalse()
        {
            Author author1 = new("Ivan", "Ivanov");
            Author author2 = new("Petr", "Petrov");

            Assert.IsFalse(author1.Equals(author2));
        }

        [DataTestMethod]
        [DataRow(null, "lastname", DisplayName = "Name is null")]
        [DataRow("", "lastname", DisplayName = "Name is empty")]
        [DataRow("name", null, DisplayName = "Lastname is null")]
        [DataRow("name", "", DisplayName = "Lastname is empty")]
        [DataRow("The following attributes and the values assigned " +
            "to them appear in the Visual Studio Properties window " +
            "for a particular test method. These attributes are not " +
            "meant to be accessed through the code of the unit test. " +
            "Instead, they affect the ways the unit test is used or " +
            "run, either by you through the IDE of Visual Studio, or " +
            "by the Visual Studio test engine. For example, some of " +
            "these attributes appear as columns in the Test Manager " +
            "window and Test Results window, which means that you can " +
            "use them to group and sort tests and test results.", 
            "lastname", DisplayName = "Name > 200")]
        [DataRow("The following attributes and the values assigned " +
            "to them appear in the Visual Studio Properties window for " +
            "a particular test method. These attributes are not meant " +
            "to be accessed through the code of th", "lastname",
            DisplayName = "Name = 201")]
        [DataRow("name", "The following attributes and the values assigned " +
                         "to them appear in the Visual Studio Properties window " +
                         "for a particular test method. These attributes are not " +
                         "meant to be accessed through the code of the unit test. " +
                         "Instead, they affect the ways the unit test is used or " +
                         "run, either by you through the IDE of Visual Studio, or " +
                         "by the Visual Studio test engine. For example, some of " +
                         "these attributes appear as columns in the Test Manager " +
                         "window and Test Results window, which means that you can " +
                         "use them to group and sort tests and test results.", 
            DisplayName = "Lastname > 200")]
        [DataRow("name", "The following attributes and the values assigned " +
            "to them appear in the Visual Studio Properties window for " +
            "a particular test method. These attributes are not meant " +
            "to be accessed through the code of th",
            DisplayName = "Lastname = 201")]
        [ExpectedException(typeof(ArgumentException))]
        public void Constructor_WhenDataIsInvalid_ThrowException(string name, string lastname)
        {
            Author author = new(name, lastname);
        }

        [DataTestMethod]
        [DataRow("name", "lastname",
            DisplayName = "Name and lastname < 200")]
        [DataRow("The following attributes and the values assigned " +
                 "to them appear in the Visual Studio Properties window for " +
                 "a particular test method. These attributes are not meant " +
                 "to be accessed through the code of ", "lastname",
            DisplayName = "Name = 199")]
        [DataRow("name","The following attributes and the values assigned " +
                 "to them appear in the Visual Studio Properties window for " +
                 "a particular test method. These attributes are not meant " +
                 "to be accessed through the code of ",
            DisplayName = "Lastname = 199")]
        public void Constructor_WhenDataIsValid_NotThrowExceptions(string name, string lastname)
        {
            Author author = new(name, lastname);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Equals_WhenOtherIsNull_ThrowException()
        {
            string name = "Ivan";
            string lastname = "Ivanov";
            Author author = new(name, lastname);

            author.Equals(null);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidCastException))]
        public void Equals_WhenObjIsNotAuthor_ThrowException()
        {
            string name = "Ivan";
            string lastname = "Ivanov";
            Author author = new(name, lastname);

            author.Equals(name);
        }

        [TestMethod]
        public void ToString_WhenAuthorInitializedCorrectly_AreEqual()
        {
            string name = "Ivan";
            string lastname = "Ivanov";
            Author author = new(name, lastname);

            Assert.AreEqual(name.ToLower() + " " + lastname.ToLower(),
                author.ToString());
        }
    }
}
