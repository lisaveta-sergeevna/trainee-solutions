﻿using System;
using System.Collections.Generic;

namespace ElectronicCatalog
{
    /// <summary>
    /// Book with ISBN-13 and some information about publication and authors.
    /// </summary>
    public class Book : IEquatable<Book>
    {
        /// <summary>
        /// Maximum length of book title.
        /// </summary>
        public static readonly int _maxTitleLength = 1000;

        /// <summary>
        /// String with ISBN-13 international format.
        /// </summary>
        public Isbn ISBN { get; }

        /// <summary>
        /// Book title string, not null or empty.
        /// </summary>
        public string Title { get; }

        /// <summary>
        /// Optional information about book publication date.
        /// </summary>
        public DateTime? PublicationDate { get; }

        private readonly List<Author> _authors = new();

        /// <summary>
        /// Copy of book authors collection.
        /// </summary>
        public Author[] Authors => _authors.ToArray();

        /// <summary>
        /// Constructor. Initialize book data, check for valid parameters.
        /// </summary>
        /// <param name="isbn">ISBN-13 international format book number.</param>
        /// <param name="title">Book title, not null or empty.</param>
        /// <param name="publicationDate">Publication date optional parameter.</param>
        /// <param name="authors">Collection of book authors, can be null.</param>
        /// <exception cref="ArgumentException">Thrown when title is null, empty or 
        /// more than _maxTitleLength, when isbn is null, empty or has wrong format.</exception>
        public Book(string isbn, string title, DateTime? publicationDate = null, 
            params Author[] authors)
        {
            if (string.IsNullOrEmpty(title))
            {
                throw new ArgumentException("Book title string can't be null or empty.");
            }

            if (title.Length > _maxTitleLength)
            {
                throw new ArgumentException($"Book title can't store more " +
                    $"than {_maxTitleLength} characters string.");
            }

            Title = title;
            ISBN = new(isbn);
            PublicationDate = publicationDate;

            if (authors is not null)
            {
                _authors = new(authors);
            }
        }

        /// <summary>
        /// Compare books equality by their ISBN number.
        /// </summary>
        /// <param name="other">Other book instance to compare to, not null.</param>
        /// <exception cref="ArgumentNullException">Thrown when other parameter is null.</exception>
        /// <returns>True - if books have th same ISBN, else - false.</returns>
        public bool Equals(Book other)
        {
            if (other is null)
            {
                throw new ArgumentNullException("Can't compare equality with" +
                                                " null reference Book instance.");
            }

            if (GetHashCode() == other.GetHashCode())
            {
                return ISBN.Equals(other.ISBN);
            }

            return false;
        }

        /// <summary>
        /// Compare books equality by their ISBN numbers.
        /// </summary>
        /// <param name="obj">Object that represents Book instance to compare with.</param>
        /// <returns>True - if books have th same ISBN, else - false.</returns>
        /// <exception cref="InvalidCastException">Thrown when obj doesn't represent a book.</exception>
        /// <exception cref="ArgumentNullException">Thrown when obj parameter is null.</exception>
        public override bool Equals(object? obj)
        {
            if (obj is Book other)
            {
                return Equals(other);
            }

            throw new InvalidCastException("Object can't be compared with Book instance, " +
                                           "because it doesn't represent the book.");
        }

        /// <summary>
        /// Hashcode of the book by it's ISBN number.
        /// </summary>
        /// <returns>Value of ISBN hashcode.</returns>
        public override int GetHashCode()
        {
            return ISBN.GetHashCode();
        }
    }
}
