﻿using System;
using System.Text.RegularExpressions;

namespace ElectronicCatalog
{
    /// <summary>
    /// ISBN-13 international format representation class.
    /// </summary>
    public sealed class Isbn : IEquatable<Isbn>
    {
        private static readonly string _pattern = @"^\d{13}$|^\d{3}-\d{1}-\d{2}-\d{6}-\d{1}$";
        private static readonly Regex _checker = new(_pattern);

        private readonly string _isbn;

        /// <summary>
        /// Constructs ISBN-13 from string and checks format.
        /// </summary>
        /// <param name="isbn">String of the following format: XXXXXXXXXXXXX or XXX-X-XX-XXXXXX-X</param>
        /// <exception cref="ArgumentException">Thrown when isbn string is null or empty.</exception>
        /// <exception cref="FormatException">Thrown when isbn string has incorrect format.</exception>
        public Isbn(string isbn)
        {
            if (string.IsNullOrEmpty(isbn))
            {
                throw new ArgumentException("ISBN can't be null or empty.");
            }

            if (!_checker.IsMatch(isbn))
            {
                throw new FormatException($"Invalid ISBN format, use XXX-X-XX-XXXXXX-X" +
                    $" or XXXXXXXXXXXXX where X from 0 to 9.");
            }

            _isbn = isbn.Replace("-", "");
        }

        /// <summary>
        /// Implicit conversion from string to ISBN by calling constructor.
        /// </summary>
        /// <param name="isbn">String of the following format.</param>
        public static implicit operator Isbn(string isbn) => new(isbn);

        /// <summary>
        /// String ISBN representation.
        /// </summary>
        /// <returns>String of the following format: XXXXXXXXXXXXX.</returns>
        public override string ToString()
        {
            return _isbn;
        }

        /// <summary>
        /// Typed equals for effective comparison.
        /// </summary>
        /// <param name="other">Isbn to compare with.</param>
        /// <returns>True - if isbn strings are equals, else - false.</returns>
        public bool Equals(Isbn other)
        {
            return _isbn.Equals(other._isbn);
        }

        /// <summary>
        /// Generic equals.
        /// </summary>
        /// <param name="obj">Object represents Isbn to compare with.</param>
        /// <returns>True - if isbn strings are equals, else - false.</returns>
        public override bool Equals(object obj)
        {
            if (obj is Isbn isbn)
            {
                return Equals(isbn);
            }

            throw new InvalidCastException($"Can't cast {obj.GetType()} to Isbn class.");
        }

        /// <summary>
        /// Hashcode of isbn string.
        /// </summary>
        /// <returns>Hash value of the stored isbn.</returns>
        public override int GetHashCode()
        {
            return _isbn.GetHashCode();
        }
    }
}
