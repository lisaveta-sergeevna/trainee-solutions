﻿using System;

namespace ElectronicCatalog
{
    /// <summary>
    /// Represents persons data, like name and lastname.
    /// </summary>
    public class Author : IEquatable<Author>
    {
        /// <summary>
        /// Maximum length of Name and Lastname strings, readonly.
        /// </summary>
        public static readonly int _maxLength = 200;

        /// <summary>
        /// Person name string, not null or empty.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Person lastname string, not null or empty.
        /// </summary>
        public string Lastname { get; }

        /// <summary>
        /// Constructor with check of the assined parameters.
        /// </summary>
        /// <param name="name">Person name string</param>
        /// <param name="lastname">Person lastname string</param>
        /// <exception cref="ArgumentException">Thrown when name or lastname 
        /// are null or empty or more than max stored length.</exception>
        public Author(string name, string lastname)
        {
            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(lastname))
            {
                throw new ArgumentException("Name and lastname can't be" +
                    " null or empty for author.");
            }

            if (name.Length > _maxLength || lastname.Length > _maxLength)
            {
                throw new ArgumentException($"Name and lastname can't store " +
                    $"more than {_maxLength} characters string.");
            }

            Name = name.ToLower();
            Lastname = lastname.ToLower();
        }

        /// <summary>
        /// Typed equality comparer for authors.
        /// </summary>
        /// <param name="other">Other author to compare with.</param>
        /// <returns>True - if other author has the same Name and Lastname, else - false.</returns>
        /// <exception cref="ArgumentNullException">Thrown when other object is null.</exception>
        public bool Equals(Author other)
        {
            if (other is null)
            {
                throw new ArgumentNullException("Can't compare author with null " +
                    "reference object.");
            }

            if (GetHashCode() == other.GetHashCode())
            {
                return Name.Equals(other.Name) && Lastname.Equals(other.Lastname);
            }

            return false;
        }

        /// <summary>
        /// Generic equality comparer for authors.
        /// </summary>
        /// <param name="obj">Other author to compare with.</param>
        /// <returns>True - if obj is author with the same Name and Lastname, else - false.</returns>
        /// <exception cref="InvalidCastException">Thrown when obj can't be casted 
        /// to Author class.</exception>
        /// <exception cref="ArgumentNullException">Thrown when obj is null</exception>
        public override bool Equals(object obj)
        {
            if (obj is Author author)
            {
                return Equals(author);
            }

            throw new InvalidCastException("Can't cast the object to author" +
                " class to compare.");
        }

        /// <summary>
        /// Hashcode of author.
        /// </summary>
        /// <returns>Sum of author name and lastname hashes.</returns>
        public override int GetHashCode()
        {
            return Name.GetHashCode() + Lastname.GetHashCode();
        }

        /// <summary>
        /// String author representation.
        /// </summary>
        /// <returns>String with author Name snd Lastname.</returns>
        public override string ToString()
        {
            return $"{Name} {Lastname}";
        }
    }
}
