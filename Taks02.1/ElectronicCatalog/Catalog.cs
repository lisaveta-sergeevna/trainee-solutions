﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ElectronicCatalog
{
    /// <summary>
    /// Catalog class represents the dictionary-like collection of Books.
    /// </summary>
    public class Catalog : Dictionary<Isbn, Book>, IEnumerable<Book>
    {
        /// <summary>
        /// Empty constructor for testing needs.
        /// </summary>
        public Catalog() { }

        /// <summary>
        /// Add all mentioned books into catalog.
        /// </summary>
        /// <param name="books">Books to add into catalog.</param>
        /// <exception cref="ArgumentNullException">Thrown when books parameter is null.</exception>
        public Catalog(IEnumerable<Book> books)
        {
            if (books is null)
            {
                throw new ArgumentNullException("Can't insert null value " +
                                                "collection into catalog.");
            }

            foreach (Book book in books)
            {
                Add(book.ISBN, book);
            }
        }

        /// <summary>
        /// Enumerator of books in catalog, sorted by lexicographical order.
        /// </summary>
        /// <returns>Enumerator for sorted Books collection.</returns>
        public new IEnumerator<Book> GetEnumerator()
        {
            List<Book> sortedBooks = Values.ToList();
            sortedBooks.Sort((book, other) 
                => string.CompareOrdinal(book.Title.ToLower(), other.Title.ToLower()));

            return sortedBooks.GetEnumerator();
        }

        /// <summary>
        /// Returns the collection of books that have mentioned author in authors property.
        /// </summary>
        /// <param name="name">Author name to search books for, register ignored.</param>
        /// <param name="lastname">Author lastname to search books for, register ignored.</param>
        /// <returns>Collection of book that have this author in authors list.</returns>
        public IEnumerable<Book> BooksByAuthor(string name, string lastname)
        {
            Author author = new(name, lastname);

            return Values.Where(book 
                => book.Authors.Contains(author));
        }

        /// <summary>
        /// Chooses the books with not null publication date and returns the collection of them.
        /// </summary>
        /// <returns>Books sorted by publication date descending order.</returns>
        public IEnumerable<Book> BooksSortedByPublicationDate()
        {
            return Values
                .Where(book => book.PublicationDate.HasValue)
                .OrderByDescending(book => book.PublicationDate.Value);
        }

        /// <summary>
        /// Collection of info about each Author and his amount of books in catalog.
        /// </summary>
        /// <returns>Tuples with (author, his amount of books).</returns>
        public IEnumerable<(Author, int)> CountBooksByAuthors()
        {
            return Values
                .Where(book => book.Authors is not null)
                .SelectMany(book => book.Authors,
                    (book, author) => (book, author))
                .GroupBy(pair => pair.author)
                .Select(group => (group.Key, group.Count()));
        }
    }
}
