﻿using System;

namespace MathAlgorithms
{
    /// <summary>
    /// Event arguments for updating the matrix.
    /// </summary>
    /// <typeparam name="T">Type of values in matrix.</typeparam>
    public class ElementChangedEventArgs<T> : EventArgs
    {
        /// <summary>
        /// Row index of changed element.
        /// </summary>
        public int Row { get; }

        /// <summary>
        /// Column number of changed element.
        /// </summary>
        public int Column { get; }

        /// <summary>
        /// Old value of changed element.
        /// </summary>
        public T OldValue { get; }

        /// <summary>
        /// New value of changed element.
        /// </summary>
        public T NewValue { get; }

        /// <summary>
        /// Constructor of event args with info about changed element.
        /// </summary>
        /// <param name="i">Row number</param>
        /// <param name="j">Column number</param>
        /// <param name="old">Old element value</param>
        /// <param name="new">New element value</param>
        public ElementChangedEventArgs(int i, int j, T old, T @new)
        {
            (Row, Column, OldValue, NewValue) = (i, j, old, @new);
        }

        /// <summary>
        /// String representation of event args information.
        /// </summary>
        /// <returns>String with information.</returns>
        public override string ToString()
        {
            return $"[{Row}, {Column}] = " +
                $"old - {OldValue}, new - {NewValue}";
        }
    }
}
