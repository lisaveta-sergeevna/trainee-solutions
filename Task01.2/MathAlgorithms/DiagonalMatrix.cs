﻿using System;
using System.Text;

namespace MathAlgorithms
{
    /// <summary>
    /// Diagonal square matrix (all elements except diagonal are default).
    /// </summary>
    /// <typeparam name="T">Type of the stoted values.</typeparam>
    public class DiagonalMatrix<T> : SquareMatrix<T>
    {
        /// <summary>
        /// Indexer of the matrix. Only diagonal values where i = j can be assigned.
        /// </summary>
        /// <param name="i">Row number, 0 < i <= Size</param>
        /// <param name="j">Column number, 0 < j <= Size</param>
        /// <returns>Value at [i, j] position in matrix.</returns>
        /// <exception cref="IndexOutOfRangeException">Thrown when row or column index
        ///  is out of the bounds.</exception>
        public override T this[int i, int j]
        {
            get
            {
                CheckIndexes(i, j);
                return i == j ? _elements[i] : default;
            }

            set
            {
                CheckIndexes(i, j);

                if (i != j)
                {
                    throw new ArgumentException("Can't set value to the non-diagonal" +
                        " element of the matrix.");
                }
                else if (!Equals(_elements[i], value))
                {
                    T old = _elements[i];
                    _elements[i] = value;

                    OnElementChanged(new(i, j, old, value));
                }
            }
        }

        /// <summary>
        /// Constructor of diagonal matrix size x size.
        /// </summary>
        /// <param name="size">Number of rows (columns).</param>
        /// <exception cref="ArgumentException">Thrown when size is 0 or less.</exception>
        public DiagonalMatrix(int size) : base()
        {
            Rank = size;
            _elements = new T[Rank];
        }
    }
}
