﻿using System;

namespace MathAlgorithms
{
    /// <summary>
    /// Tracker of matrix ElementChanged event.
    /// </summary>
    /// <typeparam name="T">Type of values in the tracked matrix.</typeparam>
    public class Tracker<T>
    {
        /// <summary>
        /// Matrix instance to track events for.
        /// </summary>
        public SquareMatrix<T> Matrix { get; }

        /// <summary>
        /// Constructor that checks matrix reference for null and subscribes to matrix event.
        /// </summary>
        /// <param name="matrix">Matrix to track, not null.</param>
        /// <exception cref="ArgumentNullException">Thrown when matrix reference
        ///  is null.</exception>
        public Tracker(SquareMatrix<T> matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException("Tracked matrix can't be null.");
            }

            Matrix = matrix;

            matrix.ElementChanged += ElementChangedHandler;
            matrix.ElementChanged += delegate (object sender, ElementChangedEventArgs<T> e)
            {
                Console.WriteLine($"Anonymous method invokation: {e}");
            };
            matrix.ElementChanged += (sender, e) 
                => Console.WriteLine($"Lambda operator invokation: {e}");
        }

        private void ElementChangedHandler(object sender, 
            ElementChangedEventArgs<T> e)
        {
            Console.WriteLine($"Method invokation: {e}");
        }
    }
}
