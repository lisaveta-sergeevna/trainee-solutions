﻿using System;
using System.Text;

namespace MathAlgorithms
{
    /// <summary>
    /// Square matrix of elements.
    /// </summary>
    /// <typeparam name="T">Type of the stoted values.</typeparam>
    public class SquareMatrix<T>
    {
        /// <summary>
        /// Matrix elements storage (one-dimension array).
        /// </summary>
        protected T[] _elements;

        /// <summary>
        /// Number of rows (columns) of the matrix.
        /// </summary>
        public int Rank 
        {
            get => Rank;

            protected set
            {
                if (value <= 0)
                {
                    throw new ArgumentException("Square matrix size can't be" +
                    " negative or zero.");
                }

                Rank = value;
            }
        }

        /// <summary>
        /// An event, happened then martix element changed through an indexer.
        /// </summary>
        public event EventHandler<ElementChangedEventArgs<T>> ElementChanged;

        /// <summary>
        /// Indexer of the matrix.
        /// </summary>
        /// <param name="i">Row number, 0 < i <= Size</param>
        /// <param name="j">Column number, 0 < j <= Size</param>
        /// <returns>Value at [i, j] position in matrix.</returns>
        /// <exception cref="IndexOutOfRangeException">Thrown when row or column index
        ///  is out of the bounds.</exception>
        public virtual T this[int i, int j] 
        { 
            get
            {
                CheckIndexes(i, j);
                return _elements[i * Rank + j];
            }

            set
            {
                CheckIndexes(i, j);

                if (!Equals(_elements[i * Rank + j], value))
                {
                    T old = _elements[i * Rank + j];
                    _elements[i * Rank + j] = value;

                    OnElementChanged(new(i, j, old, value));
                }
            }
        }

        /// <summary>
        /// Empty constructor for inheritors, doesn't allocate any memory for storage.
        /// </summary>
        protected SquareMatrix() { }

        /// <summary>
        /// Constructor of matrix size x size.
        /// </summary>
        /// <param name="size">Number of rows (columns).</param>
        /// <exception cref="ArgumentException">Thrown when size is 0 or less.</exception>
        public SquareMatrix(int size)
        {
            Rank = size;
            _elements = new T[Rank * Rank];
        }

        /// <summary>
        /// String matrix representation.
        /// </summary>
        /// <returns>Matrix values as string.</returns>
        public override string ToString()
        {
            StringBuilder matrix = new();

            for (int i = 0; i < Rank; i++)
            {
                for (int j = 0; j < Rank; j++)
                {
                    matrix.Append($"{this[i, j]}\t");
                }

                matrix.Append('\n');
            }

            return matrix.ToString();
        }

        /// <summary>
        /// Indexes checker.
        /// </summary>
        /// <param name="i">Row number, 0 <= i < Size</param>
        /// <param name="j">column number, 0 <= j < Size</param>
        /// <exception cref="IndexOutOfRangeException">Thrown when row or column index
        ///  is out of the bounds.</exception>
        protected void CheckIndexes(int i, int j)
        {
            if (i < 0 || i >= Rank || j < 0 || j >= Rank)
            {
                throw new IndexOutOfRangeException($"Index was out of range of the matrix" +
                    $" ({Rank}x{Rank} for current instance).");
            }
        }

        /// <summary>
        /// Event subscibers invokation method.
        /// </summary>
        /// <param name="e">Event arguments.</param>
        protected virtual void OnElementChanged(ElementChangedEventArgs<T> e)
        {
            ElementChanged?.Invoke(this, e);
        }
    }
}
