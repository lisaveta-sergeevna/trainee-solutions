﻿using System;

namespace MathAlgorithms
{
    internal class Program
    {
        internal static void Main(string[] args)
        {
            SquareMatrix<int> squareMatrix;
            DiagonalMatrix<double> diagonalMatrix;
            Tracker<double> tracker;

            try
            {
                squareMatrix = new(5);
                diagonalMatrix = new(3);
                tracker = new(diagonalMatrix);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }

            try
            {
                squareMatrix[0, 0] = 1;
                squareMatrix[1, 3] = -2;
                squareMatrix[2, 10] = 10;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.WriteLine(squareMatrix);

            try
            {
                diagonalMatrix[0, 0] = -3.1;
                diagonalMatrix[2, 2] = 9.3;
                diagonalMatrix[1, 1] = 0;
                diagonalMatrix[1, 2] = 30;
                diagonalMatrix[-1, 10] = 10;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.WriteLine(diagonalMatrix);
        }
    }
}
