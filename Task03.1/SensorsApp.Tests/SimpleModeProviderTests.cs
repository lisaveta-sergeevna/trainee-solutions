﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SensorsApp.Model.Base;
using SensorsApp.Model.Modes;

namespace SensorsApp.Tests
{
    [TestClass]
    public class SimpleModeProviderTests
    {
        [TestMethod]
        [ExpectedException(typeof(ObjectDisposedException))]
        public void NextValue_WhenObjectDisposed_ThrowException()
        {
            SimpleModeProvider provider = new();
            provider.Dispose();

            provider.NextValue();
        }

        [TestMethod]
        public void NextValue_WhenIntervalSet_ReturnMediumValue()
        {
            Interval interval = new(4, 10);
            using SimpleModeProvider provider = new(interval);

            Assert.AreEqual(7, provider.NextValue());
        }
    }
}
