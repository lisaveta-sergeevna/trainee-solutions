﻿using System;
using System.Collections.Generic;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SensorsApp.Model.Base;
using SensorsApp.Model.Modes;

namespace SensorsApp.Tests
{
    [TestClass]
    public class CalibrationModeProviderTests
    {
        private class SubscriberMock
        {
            public string Message { get; private set; } = "";

            public List<int> Values { get; }

            public SubscriberMock(ModeProvider provider)
            {
                provider.ValueGenerated += Provider_ValueGenerated;
                Values = new();
            }

            private void Provider_ValueGenerated(object sender, EventArgs e)
            {
                Message = "New value generated";
                Values.Add((sender as ModeProvider).NextValue());
            }
        }

        [TestMethod]
        public void Start_WhenNewValueGenerated_InvokeEventHandler()
        {
            Interval interval = new(0, 3);

            using CalibrationModeProvider provider = new(interval);
            SubscriberMock subscriber = new(provider);

            provider.Start();
            Thread.Sleep((int)(provider.Period * 2));

            Assert.AreEqual("New value generated", subscriber.Message);
        }

        [TestMethod]
        public void Start_GenerateNumbersFromIntervalInCycle()
        {
            Interval interval = new(0, 3);

            using CalibrationModeProvider provider = new(interval);
            SubscriberMock subscriber = new(provider);

            List<int> result = new() {1, 2, 3, 0, 1, 2};

            provider.Start();
            Thread.Sleep((int)(7 * provider.Period));
            provider.Stop();

            CollectionAssert.AreEquivalent(result, subscriber.Values);
        }
    }
}
