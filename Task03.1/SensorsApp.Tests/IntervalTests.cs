using Microsoft.VisualStudio.TestTools.UnitTesting;
using SensorsApp.Model.Base;

namespace SensorsApp.Tests
{
    [TestClass]
    public class IntervalTests
    {
        [TestMethod]
        public void Constructor_WhenBeginIsMoreThanEnd_SwapValues()
        {
            int begin = 10, end = 4;
            Interval interval = new(begin, end);

            Assert.AreEqual(end, interval.Begin);
            Assert.AreEqual(begin, interval.End);
        }

        [TestMethod]
        public void Constructor_WhenBeginIsLessThanEnd_SetValues()
        {
            int begin = 4, end = 9;
            Interval interval = new(begin, end);

            Assert.AreEqual(begin, interval.Begin);
            Assert.AreEqual(end, interval.End);
        }
    }
}
