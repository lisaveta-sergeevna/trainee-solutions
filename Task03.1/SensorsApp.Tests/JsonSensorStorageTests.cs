﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SensorsApp.Controller;
using SensorsApp.Model;
using SensorsApp.Model.Base;

namespace SensorsApp.Tests
{
    [TestClass]
    public class JsonSensorStorageTests
    {
        private readonly string _file = "sensors.json";

        private readonly PressureSensor _sensor = new(new(-3, 4));

        [DataTestMethod]
        [DataRow(null)]
        [DataRow("")]
        [ExpectedException(typeof(ArgumentException))]
        public void Constructor_WhenFileIsNullOrEmpty_ThrowException(string file)
        {
            new JsonSensorStorage(file);
        }

        [TestMethod]
        [ExpectedException(typeof(ObjectDisposedException))]
        public void Sensors_WhenObjectDisposed_ThrowException()
        {
            JsonSensorStorage storage = new(_file);
            storage.Dispose();

            List<ISensor> sensors = storage.Sensors;
        }

        [TestMethod]
        [ExpectedException(typeof(ObjectDisposedException))]
        public void LoadSensors_WhenObjectDisposed_ThrowException()
        {
            JsonSensorStorage storage = new(_file);
            storage.Dispose();

            storage.LoadSensors();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AddSensor_WhenSensorIsNull_ThrowException()
        {
            using JsonSensorStorage storage = new(_file);
            storage.AddSensor(null);
        }

        [TestMethod]
        [ExpectedException(typeof(ObjectDisposedException))]
        public void AddSensor_WhenObjectDisposed_ThrowException()
        {
            JsonSensorStorage storage = new(_file);
            storage.Dispose();

            storage.AddSensor(_sensor);
        }

        [TestMethod]
        public void AddSensor_WhenSensorIsValid_AddSensorToSensors()
        {
            using JsonSensorStorage storage = new(_file);
            storage.AddSensor(_sensor);

            CollectionAssert.Contains(storage.Sensors, _sensor);
        }

        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public void DeleteSensor_WhenSensorNotFound_ThrowException()
        {
            using JsonSensorStorage storage = new(_file);
            storage.DeleteSensor(Guid.Empty);
        }

        [TestMethod]
        [ExpectedException(typeof(ObjectDisposedException))]
        public void DeleteSensor_WhenObjectDisposed_ThrowException()
        {
            JsonSensorStorage storage = new(_file);
            storage.Dispose();

            storage.DeleteSensor(Guid.NewGuid());
        }

        [TestMethod]
        public void DeleteSensor_WhenSensorIsValid_DeleteSensorFromSensors()
        {
            using JsonSensorStorage storage = new(_file);

            storage.AddSensor(_sensor);
            storage.DeleteSensor(_sensor.Guid);

            CollectionAssert.DoesNotContain(storage.Sensors, _sensor);
        }
    }
}
