﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SensorsApp.Model.Base;

namespace SensorsApp.Tests
{
    [TestClass]
    public class SensorTests
    {
        private class ObserverMock : IObserver<int>
        {
            public string ValueReport { get; private set; } = "";

            public void OnCompleted() { }

            public void OnError(Exception error) { }

            public void OnNext(int value)
            {
                ValueReport = $"{value} appeared";
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ObjectDisposedException))]
        public void SwitchMode_WhenObjectDispose_ThrowException()
        {
            Sensor sensor = new();
            sensor.Dispose();

            sensor.SwitchMode();
        }

        [TestMethod]
        [ExpectedException(typeof(ObjectDisposedException))]
        public void Subscribe_WhenObjectDisposed_ThrowException()
        {
            Sensor sensor = new();
            sensor.Dispose();

            sensor.Subscribe(null);
        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Subscribe_WhenObserverIsNull_ThrowException()
        {
            using Sensor sensor = new();
            sensor.Subscribe(null);
        }

        [TestMethod]
        public void Subscribe_WhenObserverIsValid_AddItToNotifyList()
        {
            using Sensor sensor = new(new(10, 16));
            ObserverMock observer = new();

            using IDisposable unsubscriber = sensor.Subscribe(observer);

            sensor.SwitchMode();
            Thread.Sleep(1500);
            sensor.Mode.Stop();

            Assert.AreEqual("11 appeared", observer.ValueReport);
        }

        [TestMethod]
        public void DisposeUnsubscriber_RemoveItFromNotifyList()
        {
            using Sensor sensor = new(new(10, 16));
            ObserverMock observer1 = new();
            ObserverMock observer2 = new();

            using IDisposable unsubscriber2 = sensor.Subscribe(observer2);

            IDisposable unsubscriber1 = sensor.Subscribe(observer1);
            unsubscriber1.Dispose();

            sensor.SwitchMode();
            Thread.Sleep(1500);
            sensor.Mode.Stop();

            Assert.AreEqual("", observer1.ValueReport);
            Assert.AreEqual("11 appeared", observer2.ValueReport);
        }
    }
}
