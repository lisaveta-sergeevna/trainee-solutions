﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SensorsApp.Controller;

namespace SensorsApp.Tests
{
    [TestClass]
    public class SubscriberTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Constructor_WhenCellIsNull_ThrowException()
        {
            new Subscriber(null);
        }
    }
}
