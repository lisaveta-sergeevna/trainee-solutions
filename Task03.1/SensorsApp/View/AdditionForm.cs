﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SensorsApp.View
{
    /// <summary>
    /// Form that constructs new sensor and add it into storage.
    /// </summary>
    public partial class AdditionForm : Form
    {
        private readonly List<string> _types = new()
        {
            "Temperature",
            "Humidity",
            "Pressure"
        };

        public int Begin { get; private set; } = 0;

        public int End { get; private set; } = 1;

        public string Type { get; private set; }

        /// <summary>
        /// Form constructor, init form and save reference to data storage.
        /// </summary>
        /// <exception cref="ArgumentNullException">Thrown when storage is null.</exception>
        public AdditionForm()
        {
            InitializeComponent();

            typesComboBox.DataSource = _types;

            beginTextBox.Text = Begin.ToString();
            endTextBox.Text = End.ToString();
        }

        private void beginTextBox_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                Begin = int.Parse(beginTextBox.Text);
            }
            catch
            {
                e.Cancel = true;
                beginTextBox.Select(0, beginTextBox.Text.Length);

                errorProvider.SetError(beginTextBox,
                    "Interval begin value should be integer.");
            }
        }

        private void endTextBox_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                End = int.Parse(endTextBox.Text);
            }
            catch
            {
                e.Cancel = true;
                endTextBox.Select(0, endTextBox.Text.Length);

                errorProvider.SetError(endTextBox,
                    "Interval end value should be integer.");
            }
        }

        private void beginTextBox_Validated(object sender, EventArgs e)
        {
            errorProvider.Clear();
        }

        private void endTextBox_Validated(object sender, EventArgs e)
        {
            errorProvider.Clear();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            try
            {
                Type = _types[typesComboBox.SelectedIndex];
                DialogResult = DialogResult.OK;
            }
            catch (Exception exception)
            {
                MessageBox.Show($"An error occurred while " +
                                $"creating new sensor: \n{exception.Message}");
            }

            Close();
        }

        private void typesComboBox_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (sender is ComboBox box)
            {
                e.DrawBackground();

                if (e.Index >= 0)
                {
                    StringFormat format = new StringFormat();

                    if ((e.State & DrawItemState.ComboBoxEdit) == DrawItemState.ComboBoxEdit)
                    {
                        format.LineAlignment = StringAlignment.Center;
                        format.Alignment = StringAlignment.Center;
                    }

                    Brush brush = new SolidBrush(box.ForeColor);

                    if ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
                    {
                        brush = SystemBrushes.HighlightText;
                    }

                    e.Graphics.DrawString(box.Items[e.Index].ToString(),
                        box.Font, brush, e.Bounds, format);
                }
            }
        }
    }
}
