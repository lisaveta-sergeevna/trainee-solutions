﻿
namespace SensorsApp.View
{
    partial class PeriodForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.setupButton = new System.Windows.Forms.Button();
            this.periodTextBox = new System.Windows.Forms.TextBox();
            this.periodErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.periodErrorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // setupButton
            // 
            this.setupButton.Location = new System.Drawing.Point(94, 103);
            this.setupButton.Name = "setupButton";
            this.setupButton.Size = new System.Drawing.Size(188, 23);
            this.setupButton.TabIndex = 0;
            this.setupButton.Text = "Set up";
            this.setupButton.UseVisualStyleBackColor = true;
            this.setupButton.Click += new System.EventHandler(this.setupButton_Click);
            // 
            // periodTextBox
            // 
            this.periodTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.periodTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.periodTextBox.Location = new System.Drawing.Point(94, 68);
            this.periodTextBox.Multiline = true;
            this.periodTextBox.Name = "periodTextBox";
            this.periodTextBox.Size = new System.Drawing.Size(188, 20);
            this.periodTextBox.TabIndex = 1;
            this.periodTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.periodTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.periodTextBox_Validating);
            this.periodTextBox.Validated += new System.EventHandler(this.periodTextBox_Validated);
            // 
            // periodErrorProvider
            // 
            this.periodErrorProvider.ContainerControl = this;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(94, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(188, 21);
            this.label1.TabIndex = 2;
            this.label1.Text = "Enter new value for sensor period:";
            // 
            // PeriodForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(381, 156);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.periodTextBox);
            this.Controls.Add(this.setupButton);
            this.Font = new System.Drawing.Font("Myanmar Text", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "PeriodForm";
            this.Text = "Set up new period";
            ((System.ComponentModel.ISupportInitialize)(this.periodErrorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button setupButton;
        private System.Windows.Forms.TextBox periodTextBox;
        private System.Windows.Forms.ErrorProvider periodErrorProvider;
        private System.Windows.Forms.Label label1;
    }
}