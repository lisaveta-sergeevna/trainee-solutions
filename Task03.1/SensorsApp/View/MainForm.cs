﻿using System;
using System.Drawing;
using System.Windows.Forms;
using SensorsApp.Controller;

namespace SensorsApp.View
{
    /// <summary>
    /// Main data presentation form.
    /// </summary>
    public partial class MainForm : Form
    {
        private MainController _controller;

        /// <summary>
        /// Constructor that initialize form component.
        /// </summary>                                            
        public MainForm()
        {
            InitializeComponent();

            _controller = new();
        }

        #region FormEvents

        private void addFileButton_Click(object sender, EventArgs e)
        {
            using OpenFileDialog dialog = new()
            {
                Filter = _controller.GetStorageFilters(), 
                FilterIndex = 1
            };

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                string file = dialog.FileName;

                try
                {
                    _controller.LoadDataFromStorage(file, sensorsGrid);
                    SetUpDataContainerSettings(sensorsGrid);

                    for (int i = 0; i < sensorsGrid.RowCount; i++)
                    {
                        sensorsGrid.InvalidateRow(i);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Some error occurred while " +
                                    $"reading the file: \n{ex.Message}");
                }
            }
        }

        private void addSensorButton_Click(object sender, EventArgs e)
        {
            try
            {
                using AdditionForm form = new();
                form.ShowDialog();

                if (form.DialogResult == DialogResult.OK)
                {
                    _controller.AddRow(sensorsGrid, form.Type, form.Begin, form.End);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Some error occurred while " +
                                $"running addition dialog: \n{ex.Message}");
            }
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            _controller.Dispose();
        }

        private void sensorsGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewColumn column = sensorsGrid.Columns[e.ColumnIndex];

            switch (column.HeaderText)
            {
                case "Switch":
                    SwitchCellClick(e.RowIndex);
                    break;

                case "Period":
                    PeriodCellClick(e.RowIndex);
                    break;

                case "Remove":
                    RemoveCellClick(e.RowIndex);
                    break;
            }
        }

        private void SwitchCellClick(int index)
        {
            try
            {
                _controller.SwitchRowMode(index);

                sensorsGrid.EndEdit();
                sensorsGrid.InvalidateRow(index);
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Some error occurred while " +
                                $"switching mode: \n{ex.Message}");
            }
        }

        private void PeriodCellClick(int index)
        {
            try
            {
                using PeriodForm form = new(_controller.GetRowPeriod(index));
                form.ShowDialog();

                _controller.SetRowPeriod(index, form.Period);
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Some error occurred while " +
                                $"running period dialog: \n{ex.Message}");
            }
        }

        private void RemoveCellClick(int index)
        {
            try
            {
                _controller.RemoveRow(sensorsGrid, index);
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Some error occurred while " +
                                $"removing sensor from storage: \n{ex.Message}");
            }
        }

        private void saveAllButton_Click(object sender, EventArgs e)
        {
            _controller.SaveAll();
        }

        private void saveAsButton_Click(object sender, EventArgs e)
        {
            using OpenFileDialog dialog = new()
            {
                Filter = _controller.GetStorageFilters(),
                FilterIndex = 1
            };

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                string file = dialog.FileName;

                try
                {
                    _controller.SaveAs(file);
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Some error occurred while " +
                                    $"saving the file: \n{ex.Message}");
                }
            }
        }

        #endregion

        private void SetUpDataContainerSettings(DataGridView dataContainer)
        {
            (Image image, string header)[] columns =
            {
                (Properties.Images.switch_mode, "Switch"),
                (Properties.Images.period_switch, "Period"),
                (Properties.Images.delete_sensor, "Remove")
            };

            foreach ((Image image, string header) in columns)
            {
                DataGridViewImageColumn column = new()
                {
                    Image = new Bitmap(image, 20, 20),
                    AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader,
                    HeaderText = header
                };

                dataContainer.Columns.Add(column);
            }
        }
    }
}
