﻿using System.Windows.Forms;
using System.ComponentModel;

namespace SensorsApp.View
{
    /// <summary>
    /// Period dialog to set new sensor period value.
    /// </summary>
    public partial class PeriodForm : Form
    {
        public uint Period { get; private set; }

        /// <summary>
        /// Constructor init the form and set default periodTextBox value.
        /// </summary>
        public PeriodForm(uint period)
        {
            InitializeComponent();

            periodTextBox.Text = period.ToString();
            periodTextBox.Select(0, periodTextBox.Text.Length);
        }

        private void periodTextBox_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                Period = uint.Parse(periodTextBox.Text);
            }
            catch
            {
                e.Cancel = true;
                periodTextBox.Select(0, periodTextBox.Text.Length);

                periodErrorProvider.SetError(periodTextBox, 
                    "Period value should be positive integer.");
            }
        }

        private void periodTextBox_Validated(object sender, System.EventArgs e)
        {
            periodErrorProvider.Clear();
        }

        private void setupButton_Click(object sender, System.EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
