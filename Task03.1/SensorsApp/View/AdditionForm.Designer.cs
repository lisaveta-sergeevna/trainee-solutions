﻿
namespace SensorsApp.View
{
    partial class AdditionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.typesComboBox = new System.Windows.Forms.ComboBox();
            this.beginTextBox = new System.Windows.Forms.TextBox();
            this.endTextBox = new System.Windows.Forms.TextBox();
            this.addButton = new System.Windows.Forms.Button();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Myanmar Text", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.label1.Location = new System.Drawing.Point(88, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Sensor type";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Myanmar Text", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label2.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.label2.Location = new System.Drawing.Point(88, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 24);
            this.label2.TabIndex = 1;
            this.label2.Text = "Interval";
            // 
            // typesComboBox
            // 
            this.typesComboBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.typesComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.typesComboBox.Location = new System.Drawing.Point(190, 34);
            this.typesComboBox.Name = "typesComboBox";
            this.typesComboBox.Size = new System.Drawing.Size(180, 31);
            this.typesComboBox.TabIndex = 2;
            this.typesComboBox.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.typesComboBox_DrawItem);
            // 
            // beginTextBox
            // 
            this.beginTextBox.Location = new System.Drawing.Point(190, 84);
            this.beginTextBox.Multiline = true;
            this.beginTextBox.Name = "beginTextBox";
            this.beginTextBox.Size = new System.Drawing.Size(80, 25);
            this.beginTextBox.TabIndex = 3;
            this.beginTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.beginTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.beginTextBox_Validating);
            this.beginTextBox.Validated += new System.EventHandler(this.beginTextBox_Validated);
            // 
            // endTextBox
            // 
            this.endTextBox.Location = new System.Drawing.Point(290, 84);
            this.endTextBox.Multiline = true;
            this.endTextBox.Name = "endTextBox";
            this.endTextBox.Size = new System.Drawing.Size(80, 25);
            this.endTextBox.TabIndex = 4;
            this.endTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.endTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.endTextBox_Validating);
            this.endTextBox.Validated += new System.EventHandler(this.endTextBox_Validated);
            // 
            // addButton
            // 
            this.addButton.Font = new System.Drawing.Font("Myanmar Text", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.addButton.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.addButton.Location = new System.Drawing.Point(88, 126);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(282, 27);
            this.addButton.TabIndex = 5;
            this.addButton.Text = "Save";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // AdditionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(470, 185);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.endTextBox);
            this.Controls.Add(this.beginTextBox);
            this.Controls.Add(this.typesComboBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Myanmar Text", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "AdditionForm";
            this.Text = "Add new sensor";
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox typesComboBox;
        private System.Windows.Forms.TextBox beginTextBox;
        private System.Windows.Forms.TextBox endTextBox;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.ErrorProvider errorProvider;
    }
}