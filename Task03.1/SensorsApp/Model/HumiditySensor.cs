﻿using System;
using System.Drawing;
using SensorsApp.Model.Base;

namespace SensorsApp.Model
{
    /// <summary>
    /// Some humidity sensor.
    /// </summary>
    public class HumiditySensor : Sensor
    {
        public override Bitmap Icon => 
            new(Properties.Images.humidity_sensor, 20, 20);

        public HumiditySensor(Interval interval) : base(interval)
        { }

        public HumiditySensor(Guid id, Interval interval) : base(id, interval)
        { }
    }
}
