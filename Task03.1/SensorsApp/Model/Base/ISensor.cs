﻿using System;
using System.Drawing;
using SensorsApp.Model.Modes;

namespace SensorsApp.Model.Base
{
    /// <summary>
    /// Basic Sensor functionality.
    /// </summary>
    public interface ISensor : IDisposable, IObservable<int>
    {
        /// <summary>
        /// Some sensor icon.
        /// </summary>
        public Bitmap Icon { get; }

        /// <summary>
        /// Unique identifier of sensor.
        /// </summary>
        public Guid Guid { get; }

        /// <summary>
        /// Measurement value interval.
        /// </summary>
        public Interval Interval { get; set; }

        /// <summary>
        /// Sensor working mode provider.
        /// </summary>
        public ModeProvider Mode { get; }

        /// <summary>
        /// Current value detected by sensor.
        /// </summary>
        public int Value { get; }

        /// <summary>
        /// Cyclic mode switch.
        /// </summary>
        void SwitchMode();
    }
}
