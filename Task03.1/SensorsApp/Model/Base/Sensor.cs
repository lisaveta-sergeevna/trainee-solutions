﻿using System;
using System.Collections.Generic;
using System.Drawing;
using SensorsApp.Model.Modes;

namespace SensorsApp.Model.Base
{
    /// <summary>
    /// Some standard sensor example.
    /// </summary>
    public class Sensor : ISensor
    {
        private Interval _interval;

        private int _nextMode;

        private readonly List<ModeProvider> _modes = new()
        {
            new SimpleModeProvider(),
            new CalibrationModeProvider(),
            new WorkModeProvider()
        };

        private bool _isDisposed;

        private readonly List<IObserver<int>> _observers = new();

        /// <summary>
        /// Unique sensor identifier.
        /// </summary>
        public Guid Guid { get; }

        /// <summary>
        /// Measurement value interval.
        /// </summary>
        public Interval Interval
        {
            get => _interval;

            set
            {
                foreach (ModeProvider provider in _modes)
                {
                    provider.Interval = value;
                }

                _interval = value;
            }
        }

        /// <summary>
        /// Current sensor mode.
        /// </summary>
        public ModeProvider Mode { get; private set; }

        /// <summary>
        /// Current value detected by sensor.
        /// </summary>
        public int Value => Mode.NextValue();

        /// <summary>
        /// Some sensor image for graphical representation.
        /// </summary>
        public virtual Bitmap Icon => 
            new(Properties.Images.sensor, 20, 20);

        /// <summary>
        /// Constructor, sets sensor guid, initial sensor mode
        /// and subscribes to Mode event.
        /// </summary>
        public Sensor()
        {
            IdGenerator generator = IdGenerator.GetGenerator();
            Guid = generator.GetNewId();

            _isDisposed = false;

            foreach (ModeProvider provider in _modes)
            {
                provider.ValueGenerated += Notify;
            }

            SwitchMode();
        }

        /// <summary>
        /// Sets measurement interval.
        /// </summary>
        /// <param name="interval">Interval of measured values.</param>
        public Sensor(Interval interval) : this()
        {
            Interval = interval;
        }

        /// <summary>
        /// Sensor constructor with guid.
        /// </summary>
        /// <param name="id">Guid of the sensor.</param>
        /// <param name="interval">Interval of measurement values.</param>
        public Sensor(Guid id, Interval interval) : this(interval)
        {
            Guid = id;
        }

        /// <summary>
        /// Cyclic switch of sensor mode.
        /// </summary>
        public void SwitchMode()
        {
            if (_isDisposed)
            {
                throw new ObjectDisposedException("Sensor is disposed.");
            }

            Mode?.Stop();
            Mode = _modes[_nextMode];
            Mode.Start();

            _nextMode++;
            _nextMode %= _modes.Count;
        }

        /// <summary>
        /// Dispose sensor object correctly, release all resources.
        /// </summary>
        public void Dispose()
        {
            foreach (ModeProvider provider in _modes)
            {
                provider.Dispose();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// Subscribe to sensor value changing.
        /// </summary>
        /// <param name="observer">Observer that subscribes to event.</param>
        /// <returns>Some disposable object, where Dispose should be called to unsubscribe.</returns>
        public IDisposable Subscribe(IObserver<int> observer)
        {
            if (_isDisposed)
            {
                throw new ObjectDisposedException("Sensor disposed.");
            }

            if (observer is null)
            {
                throw new ArgumentNullException("Can't subscribe null" +
                                                " reference observer to Sensor events.");
            }

            _observers.Add(observer);
            return new Unsubscriber(_observers, observer);
        }

        private void Notify(object sender, EventArgs e)
        {
            foreach (IObserver<int> observer in _observers)
            {
                observer.OnNext(Value);
            }
        }

        private class Unsubscriber : IDisposable
        {
            private readonly IObserver<int> _observer;

            private readonly IList<IObserver<int>> _observers;

            public Unsubscriber(IList<IObserver<int>> observers,
                IObserver<int> observer)
            {
                _observers = observers;
                _observer = observer;
            }

            public void Dispose()
            {
                _observers.Remove(_observer);
            }
        }
    }
}
