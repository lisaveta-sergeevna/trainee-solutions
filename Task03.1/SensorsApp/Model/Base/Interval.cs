﻿namespace SensorsApp.Model.Base
{
    /// <summary>
    /// Sensor measurement interval.
    /// </summary>
    public readonly struct Interval
    {
        /// <summary>
        /// Minimum allowed value of the interval.
        /// </summary>
        public int Begin { get; }

        /// <summary>
        /// Maximum allowed value of the interval.
        /// </summary>
        public int End { get; }

        /// <summary>
        /// Constructs the interval of values.
        /// </summary>
        /// <param name="begin">Begin minimum value.</param>
        /// <param name="end">End maximum value.</param>
        public Interval(int begin, int end)
        {
            if (begin > end)
            {
                Begin = end;
                End = begin;
            }
            else
            {
                Begin = begin;
                End = end;
            }
        }

        /// <summary>
        /// Represents interval as formatted string.
        /// </summary>
        /// <returns>String contains interval representation.</returns>
        public override string ToString()
        {
            return $"[{Begin}, {End}]";
        }

        /// <summary>
        /// Parse interval from string.
        /// </summary>
        /// <param name="interval">String of the following format "[a, b]" where a, b - numbers.</param>
        /// <returns>New interval struct.</returns>
        public static Interval Parse(string interval)
        {
            string[] parts = interval
                .Replace("[", "")
                .Replace("]", "")
                .Replace(" ", "")
                .Split(',');

            return new(
                int.Parse(parts[0]), 
                int.Parse(parts[1]));
        }
    }
}
