﻿using System;

namespace SensorsApp.Model.Base
{
    /// <summary>
    /// Sensor IdGenerator, represents a thread-safe singleton pattern.
    /// </summary>
    public class IdGenerator
    {
        private static readonly IdGenerator _instance = new();

        static IdGenerator() { }

        private IdGenerator() { }

        /// <summary>
        /// Static generator instance constructing.
        /// </summary>
        /// <returns>Single instance of IdGenerator.</returns>
        public static IdGenerator GetGenerator() => _instance;

        /// <summary>
        /// Generates new unique Id.
        /// </summary>
        /// <returns>New Guid.</returns>
        public Guid GetNewId() => Guid.NewGuid();
    }
}
