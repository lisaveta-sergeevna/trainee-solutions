﻿using System;
using System.Collections.Generic;

namespace SensorsApp.Model.Base
{
    /// <summary>
    /// Abstract sensors storage, abstract factory pattern is used.
    /// </summary>
    public interface ISensorStorage : IDisposable
    {
        /// <summary>
        /// Stored sensors collection.
        /// </summary>
        public List<ISensor> Sensors { get; }

        /// <summary>
        /// Loading sensors from storage.
        /// </summary>
        public void LoadSensors();

        /// <summary>
        /// Create new sensor object and save it to storage.
        /// </summary>
        /// <param name="sensor">Some sensor to add into storage.</param>
        public void AddSensor(ISensor sensor);

        /// <summary>
        /// Delete sensor from storage by it's guid.
        /// </summary>
        /// <param name="sensorGuid">Guid of sensor to delete.</param>
        public void DeleteSensor(Guid sensorGuid);

        /// <summary>
        /// Save local sensor data into storage.
        /// </summary>
        public void Save();
    }
}
