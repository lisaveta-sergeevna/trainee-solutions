﻿using System.Threading;
using SensorsApp.Model.Base;

namespace SensorsApp.Model.Modes
{
    /// <summary>
    /// Calibration mode. Generates numbers of the interval sequential one at a second.
    /// </summary>
    public class CalibrationModeProvider : ModeProvider
    {
        private int _counter;

        private Timer _timer;

        /// <summary>
        /// Allocate memory for mode provider.
        /// </summary>
        public CalibrationModeProvider()
        {
            Period = 1000;
        }

        /// <summary>
        /// Constructor, sets values interval.
        /// </summary>
        /// <param name="interval">Interval of generated values.</param>
        public CalibrationModeProvider(Interval interval) : base(interval)
        {
            Period = 1000;
        }

        /// <summary>
        /// Start generating values.
        /// </summary>
        public override void Start()
        {
            _counter = Interval.Begin;
            _timer = new(GenerateNext, null, Period, Period);
        }

        /// <summary>
        /// Stop generating values.
        /// </summary>
        public override void Stop()
        {
            _timer?.Dispose();
        }

        /// <summary>
        /// Next value accessor.
        /// </summary>
        /// <returns>Current value.</returns>
        public override int NextValue()
        {
            CheckDisposed();
            return _counter;
        }

        /// <summary>
        /// Dispose mode object, stop value generation.
        /// </summary>
        public override void Dispose()
        {
            Stop();
            base.Dispose();
        }

        /// <summary>
        /// Mode name.
        /// </summary>
        /// <returns>String with mode name.</returns>
        public override string ToString()
        {
            return "calibration";
        }

        private void GenerateNext(object state)
        {
            _counter++;

            if (_counter > Interval.End)
            {
                _counter = Interval.Begin;
            }

            OnValueGenerated(new());
        }
    }
}
