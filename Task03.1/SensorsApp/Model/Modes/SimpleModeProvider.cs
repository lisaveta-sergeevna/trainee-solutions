﻿using SensorsApp.Model.Base;

namespace SensorsApp.Model.Modes
{
    /// <summary>
    /// Simple working mode.
    /// Doesn't generate new values, always return medium value.
    /// </summary>
    public class SimpleModeProvider : ModeProvider
    {
        /// <summary>
        /// Empty constructor. Just allocates memory for provider.
        /// </summary>
        public SimpleModeProvider()
        { }

        /// <summary>
        /// Standard constructor.
        /// </summary>
        /// <param name="interval">Interval of generated values.</param>
        public SimpleModeProvider(Interval interval) : base(interval)
        { }

        /// <summary>
        /// Mode name.
        /// </summary>
        /// <returns>String with mode name.</returns>
        public override string ToString()
        {
            return "simple";
        }
    }
}
