﻿using System;
using SensorsApp.Model.Base;

namespace SensorsApp.Model.Modes
{
    /// <summary>
    /// Sensor working mode base.
    /// </summary>
    public abstract class ModeProvider : IDisposable
    {
        /// <summary>
        /// True if object is disposed and can't be used.
        /// </summary>
        protected bool _isDisposed = false;

        /// <summary>
        /// Interval of measurement values.
        /// </summary>
        public Interval Interval { get; set; }

        /// <summary>
        /// Period of new value generation.
        /// </summary>
        public uint Period { get; set; }

        /// <summary>
        /// Event happened when next value is generated.
        /// </summary>
        public event EventHandler<EventArgs> ValueGenerated;

        /// <summary>
        /// Constructs an empty Mode.
        /// </summary>
        protected ModeProvider() { }

        /// <summary>
        /// Constructor that init values interval.
        /// </summary>
        /// <param name="interval">Interval of allowed values.</param>
        protected ModeProvider(Interval interval)
        {
            Interval = interval;
        }

        /// <summary>
        /// Start mode work.
        /// </summary>
        public virtual void Start() { }

        /// <summary>
        /// Stop mode work.
        /// </summary>
        public virtual void Stop() { }

        /// <summary>
        /// Next value accessor.
        /// </summary>
        /// <returns>Current value.</returns>
        public virtual int NextValue()
        {
            return (Interval.End - Interval.Begin) / 2 + Interval.Begin;
        }

        /// <summary>
        /// Dispose mode object.
        /// </summary>
        public virtual void Dispose()
        {
            _isDisposed = true;
        }

        /// <summary>
        /// Method to call ValueGenerated event subscribers.
        /// </summary>
        /// <param name="e">Event arguments.</param>
        protected virtual void OnValueGenerated(EventArgs e)
        {
            ValueGenerated?.Invoke(this, e);
        }

        /// <summary>
        /// Check isDisposed method.
        /// </summary>
        /// <exception cref="ObjectDisposedException">Thrown when isDisposed is true.</exception>
        protected void CheckDisposed()
        {
            if (_isDisposed)
            {
                throw new ObjectDisposedException("Mode object" +
                                                  " is disposed, can't " +
                                                  "get next value.");
            }
        }
    }
}
