﻿using System;
using System.Threading;
using SensorsApp.Model.Base;

namespace SensorsApp.Model.Modes
{
    /// <summary>
    /// Work mode provider, generates new random value from the interval periodically.
    /// </summary>
    public class WorkModeProvider : ModeProvider
    {
        private readonly Random _random = new();

        private Timer _timer;

        private int _value;

        /// <summary>
        /// Allocate memory for mode provider.
        /// </summary>
        public WorkModeProvider()
        { }

        /// <summary>
        /// Constructor, sets interval of values and period of new value generating.
        /// </summary>
        /// <param name="interval">Interval of allowed generated values.</param>
        /// <param name="period">Period af new value generation.</param>
        public WorkModeProvider(Interval interval, uint period) : base(interval)
        {
            Period = period;
            _value = interval.Begin;
        }

        /// <summary>
        /// Start value generating.
        /// </summary>
        public override void Start()
        {
            _timer = new(GenerateNext, null, 0, Period);
        }

        /// <summary>
        /// Stop generating values.
        /// </summary>
        public override void Stop()
        {
            _timer?.Dispose();
        }

        /// <summary>
        /// Next value accessor.
        /// </summary>
        /// <returns>Current value.</returns>
        public override int NextValue()
        {
            CheckDisposed();
            return _value;
        }

        /// <summary>
        /// Dispose mode object, stop value generation.
        /// </summary>
        public override void Dispose()
        {
            Stop();
            base.Dispose();
        }

        /// <summary>
        /// Mode name.
        /// </summary>
        /// <returns>String with mode name.</returns>
        public override string ToString()
        {
            return "work";
        }

        private void GenerateNext(object state)
        {
            _value = _random.Next(Interval.End - Interval.Begin) 
                     + Interval.Begin;

            OnValueGenerated(new());
        }
    }
}
