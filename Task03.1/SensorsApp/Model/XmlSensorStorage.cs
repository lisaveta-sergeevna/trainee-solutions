﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Linq;
using SensorsApp.Model.Base;

namespace SensorsApp.Model
{
    /// <summary>
    /// Storage of sensors in xml file.
    /// </summary>
    public class XmlSensorStorage : ISensorStorage
    {
        private readonly string _file;

        private XDocument _document;

        private bool _isDisposed;

        private List<ISensor> _sensors;

        private List<ISensor> _added = new();

        private List<Guid> _removed = new();

        #region XmlNames

        private readonly string _sensor = "sensor";

        private readonly string _type = "type";

        private readonly string _id = "id";

        private readonly string _interval = "interval";

        private readonly string _begin = "begin";

        private readonly string _end = "end";

        #endregion

        /// <summary>
        /// Xml storage file filter.
        /// </summary>
        public static string FileFilter => "XML files (*.xml)|*.xml";

        /// <summary>
        /// List collection of stored sensors.
        /// </summary>
        /// <exception cref="ObjectDisposedException">Thrown when object is disposed before method call.</exception>
        public List<ISensor> Sensors
        {
            get
            {
                if (_isDisposed)
                {
                    throw new ObjectDisposedException("Storage disposed.");
                }

                return new(_sensors);
            }
        }

        /// <summary>
        /// Constructs xml storage for sensors.
        /// </summary>
        /// <param name="file">File to work with.</param>
        /// <exception cref="ArgumentException">Thrown when file string is null or empty.</exception>
        public XmlSensorStorage(string file)
        {
            if (string.IsNullOrEmpty(file))
            {
                throw new ArgumentException("Can't store sensors, " +
                                            "file name can't be null or empty.");
            }

            _file = file;

            _isDisposed = false;
            _sensors = new();
        }

        /// <summary>
        /// Load sensors from storage.
        /// </summary>
        /// <exception cref="ObjectDisposedException">Thrown when object is disposed before method call.</exception>
        public void LoadSensors()
        {
            if (_isDisposed)
            {
                throw new ObjectDisposedException("Storage disposed.");
            }

            _document = XDocument.Load(_file);

            _sensors = new(
                from sensor in _document.Root.Elements()

                let type = Type.GetType(sensor.Attribute(_type).Value)
                let id = Guid.Parse(sensor.Element(_id).Value)
                let interval = sensor.Element(_interval)
                let begin = int.Parse(interval.Element(_begin).Value)
                let end = int.Parse(interval.Element(_end).Value)

                select Activator.CreateInstance(
                    type,
                    id,
                    new Interval(begin, end))
                    as ISensor);
        }

        /// <summary>
        /// Add new sensor into storage.
        /// </summary>
        /// <param name="sensor">Sensor to add.</param>
        /// <exception cref="ArgumentNullException">Thrown when sensor is null.</exception>
        /// <exception cref="ObjectDisposedException">Thrown when object is disposed before method call.</exception>
        public void AddSensor(ISensor sensor)
        {
            if (_isDisposed)
            {
                throw new ObjectDisposedException("Storage disposed.");
            }

            if (sensor is null)
            {
                throw new ArgumentNullException("Can't add null " +
                                                "reference value into storage.");
            }

            _sensors.Add(sensor);
            _added.Add(sensor);
        }

        /// <summary>
        /// Delete sensor from storage by guid.
        /// </summary>
        /// <param name="sensorGuid">Id of sensor to delete.</param>
        /// <exception cref="KeyNotFoundException">Thrown when no such sensor found.</exception>
        /// <exception cref="ObjectDisposedException">Thrown when object is disposed before method call.</exception>
        public void DeleteSensor(Guid sensorGuid)
        {
            if (_isDisposed)
            {
                throw new ObjectDisposedException("Storage disposed.");
            }

            ISensor sensor = _sensors.FirstOrDefault(s => s.Guid.Equals(sensorGuid));

            if (sensor is not null)
            {
                _sensors.Remove(sensor);
                _removed.Add(sensorGuid);
                sensor.Dispose();

                return;
            }

            throw new KeyNotFoundException("No such sensor in storage.");
        }

        /// <summary>
        /// Save local sensors collection into storage.
        /// </summary>
        /// <exception cref="ObjectDisposedException">Thrown when current storage disposed.</exception>
        public void Save()
        {
            if (_isDisposed)
            {
                throw new ObjectDisposedException("Storage disposed.");
            }

            try
            {
                foreach (ISensor sensor in _added)
                {
                    _document.Root.Add(new XElement(_sensor,
                        new XAttribute(_type, sensor.GetType().FullName),
                        new XElement(_id, sensor.Guid),
                        new XElement(_interval,
                            new XElement(_begin, sensor.Interval.Begin),
                            new XElement(_end, sensor.Interval.End))));
                }

                _document.Root
                    .Elements()
                    .Where(e => _removed.Contains(new(e.Element(_id).Value)))
                    .Remove();

                _document.Save(_file);
            }
            catch
            {
                CloseConnection();
                LoadSensors();

                throw;
            }
            finally
            {
                _added = new();
                _removed = new();
            }
        }

        /// <summary>
        /// Release storage resources.
        /// </summary>
        public void Dispose()
        {
            CloseConnection();
            _isDisposed = true;
        }

        private void CloseConnection()
        {
            foreach (ISensor sensor in Sensors)
            {
                sensor.Dispose();
            }
        }
    }
}
