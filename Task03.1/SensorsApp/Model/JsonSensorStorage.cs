﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using SensorsApp.Model.Base;

namespace SensorsApp.Model
{
    /// <summary>
    /// Json sensors storage.
    /// </summary>
    public class JsonSensorStorage : ISensorStorage
    {
        private readonly string _file;

        private readonly string _tempPrefix = "temp";

        private readonly string _backupFile = "backup.json";

        private JsonDocument _document;

        private List<ISensor> _sensors;

        private bool _isDisposed;

        private readonly JsonWriterOptions _options = new()
        {
            Indented = true
        };

        #region JsonNames

        private readonly string _type = "type";

        private readonly string _id = "id";

        private readonly string _interval = "interval";

        private readonly string _begin = "begin";

        private readonly string _end = "end";

        #endregion

        /// <summary>
        /// Search filter for json storage files.
        /// </summary>
        public static string FileFilter => "JSON files (*.json)|*.json";

        /// <summary>
        /// List collection of stored sensors.
        /// </summary>
        /// <exception cref="ObjectDisposedException">Thrown when object is disposed before method call.</exception>
        public List<ISensor> Sensors
        {
            get
            {
                if (_isDisposed)
                {
                    throw new ObjectDisposedException("Storage disposed.");
                }

                return new(_sensors);
            }
        }

        /// <summary>
        /// Constructs the storage object in json format in file.
        /// </summary>
        /// <param name="file">File to store sensors data.</param>
        /// <exception cref="ArgumentException">Thrown when file is null of empty.</exception>
        public JsonSensorStorage(string file)
        {
            if (string.IsNullOrEmpty(file))
            {
                throw new ArgumentException("Filename can't be null or empty.");
            }

            _file = file;

            _isDisposed = false;
            _sensors = new();
        }

        /// <summary>
        /// Load sensors from json file.
        /// </summary>
        /// <exception cref="ObjectDisposedException">Thrown when object is disposed before method call.</exception>
        public void LoadSensors()
        {
            if (_isDisposed)
            {
                throw new ObjectDisposedException("Storage disposed.");
            }

            using FileStream stream = File.Open(_file, FileMode.OpenOrCreate);
            _document = JsonDocument.Parse(stream);

            _sensors = new(
                from sensor in _document.RootElement.EnumerateArray()

                let type = Type.GetType(sensor.GetProperty(_type).GetString())
                let id = sensor.GetProperty(_id).GetGuid()
                let interval = sensor.GetProperty(_interval)
                let begin = interval.GetProperty(_begin).GetInt32()
                let end = interval.GetProperty(_end).GetInt32()

                select Activator.CreateInstance(
                        type,
                        id,
                        new Interval(begin, end))
                        as ISensor);
        }

        /// <summary>
        /// Add new sensor instance into storage.
        /// </summary>
        /// <param name="sensor">Sensor to add into storage.</param>
        /// <exception cref="ObjectDisposedException">Thrown when object is disposed before method call.</exception>
        /// <exception cref="ArgumentNullException">Thrown when sensor is null.</exception>
        public void AddSensor(ISensor sensor)
        {
            if (_isDisposed)
            {
                throw new ObjectDisposedException("Storage disposed.");
            }

            if (sensor is null)
            {
                throw new ArgumentNullException("Can't add null reference" +
                                                " object into storage.");
            }

            _sensors.Add(sensor);
        }

        /// <summary>
        /// Delete some sensor from storage by it's guid.
        /// </summary>
        /// <param name="sensorGuid">Guid of sensor to delete.</param>
        /// <exception cref="ObjectDisposedException">Thrown when object is disposed before method call.</exception>
        /// <exception cref="KeyNotFoundException">Thrown when no such sensor found in storage.</exception>
        public void DeleteSensor(Guid sensorGuid)
        {
            if (_isDisposed)
            {
                throw new ObjectDisposedException("Storage disposed.");
            }

            ISensor sensor = _sensors.FirstOrDefault(s => s.Guid.Equals(sensorGuid));

            if (sensor is not null)
            {
                _sensors.Remove(sensor);
                sensor.Dispose();

                return;
            }

            throw new KeyNotFoundException("No such sensor in storage.");
        }

        /// <summary>
        /// Save local sensors collection into storage.
        /// </summary>
        /// <exception cref="ObjectDisposedException">Thrown when current storage disposed.</exception>
        public void Save()
        {
            if (_isDisposed)
            {
                throw new ObjectDisposedException("Storage disposed.");
            }

            try
            {
                File.Copy(_file, _backupFile, true);

                using FileStream stream = File.Create(_file);
                using Utf8JsonWriter writer = new(stream, _options);

                JsonElement root = _document.RootElement;

                writer.WriteStartArray();

                foreach (ISensor sensor in _sensors)
                {
                    AppendSensorToDocument(writer, sensor);
                }

                writer.WriteEndArray();
                writer.Flush();
            }
            catch
            {
                RestoreFile();
                throw;
            }
        }

        /// <summary>
        /// Release all json document storage resources.
        /// </summary>
        public void Dispose()
        {
            CloseConnection();
            _isDisposed = true;
        }

        private void CloseConnection()
        {
            _document?.Dispose();

            foreach (ISensor sensor in _sensors)
            {
                sensor.Dispose();
            }
        }

        private void RestoreFile()
        {
            CloseConnection();

            try
            {
                File.Replace(_backupFile,
                    _file,
                    _tempPrefix + _file);
            }
            catch { }

            LoadSensors();
        }

        private void AppendSensorToDocument(Utf8JsonWriter writer, ISensor sensor)
        {
            writer.WriteStartObject();

            writer.WriteString(_type, sensor.GetType().FullName);
            writer.WriteString(_id, sensor.Guid);

            writer.WriteStartObject(_interval);

            writer.WriteNumber(_begin, sensor.Interval.Begin);
            writer.WriteNumber(_end, sensor.Interval.End);

            writer.WriteEndObject();

            writer.WriteEndObject();
        }
    }
}
