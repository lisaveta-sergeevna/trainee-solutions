﻿using System;
using System.Drawing;
using SensorsApp.Model.Base;

namespace SensorsApp.Model
{
    /// <summary>
    /// Some pressure sensor.
    /// </summary>
    public class PressureSensor : Sensor
    {
        public override Bitmap Icon => 
            new(Properties.Images.pressure_sensor, 20, 20);

        public PressureSensor(Interval interval) : base(interval)
        { }

        public PressureSensor(Guid id, Interval interval) : base(id, interval)
        { }
    }
}
