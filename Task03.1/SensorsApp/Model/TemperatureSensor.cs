﻿using System;
using System.Drawing;
using SensorsApp.Model.Base;

namespace SensorsApp.Model
{
    /// <summary>
    /// Some temperature sensor.
    /// </summary>
    public class TemperatureSensor : Sensor
    {
        public override Bitmap Icon => 
            new(Properties.Images.temperature_sensor, 20, 20);

        public TemperatureSensor(Interval interval) : base(interval)
        { }

        public TemperatureSensor(Guid id, Interval interval) : base(id, interval)
        { }
    }
}
