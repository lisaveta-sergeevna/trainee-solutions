﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using SensorsApp.Model;
using SensorsApp.Model.Base;

namespace SensorsApp.Controller
{
    public class MainController : IDisposable
    {
        private ISensorStorage _storage;

        private List<IDisposable> _unsubscribers;

        public MainController() { }

        public string GetStorageFilters()
        {
            return XmlSensorStorage.FileFilter + "|" + JsonSensorStorage.FileFilter;
        }

        public void LoadDataFromStorage(string storageName, DataGridView dataContainer)
        {
            if (string.IsNullOrEmpty(storageName))
            {
                throw new ArgumentException("Can't work with null" +
                                            " or empty storage name.");
            }

            _storage?.Dispose();
            _storage = storageName.Contains(".xml")
                ? new XmlSensorStorage(storageName)
                : new JsonSensorStorage(storageName);

            _storage.LoadSensors();

            BindStorageWith(dataContainer);
        }

        public void SwitchRowMode(int index)
        {
            _storage.Sensors[index].SwitchMode();
        }

        public uint GetRowPeriod(int index)
        {
            ISensor sensor = _storage.Sensors[index];
            sensor.Mode.Stop();

            return sensor.Mode.Period;
        }

        public void SetRowPeriod(int index, uint period)
        {
            ISensor sensor = _storage.Sensors[index];

            sensor.Mode.Period = period;
            sensor.Mode.Start();
        }

        public void AddRow(DataGridView dataContainer, 
            string type, int begin, int end)
        {
            ISensor sensor = CreateSensor(type, begin, end);

            _storage.AddSensor(sensor);
            dataContainer.DataSource = _storage.Sensors;

            _unsubscribers.Add(SubscribeToSensor(sensor, 
                dataContainer.Rows[^1].Cells["Value"]));
        }

        public void RemoveRow(DataGridView dataContainer, int index)
        {
            _storage.DeleteSensor(_storage.Sensors[index].Guid);
            dataContainer.DataSource = _storage.Sensors;
        }

        public void SaveAll()
        {
            _storage.Save();
        }

        public void SaveAs(string storageName)
        {
            if (string.IsNullOrEmpty(storageName))
            {
                throw new ArgumentException("Can't work with null" +
                                            " or empty storage name.");
            }

            using ISensorStorage storage = storageName.Contains(".xml")
                ? new XmlSensorStorage(storageName)
                : new JsonSensorStorage(storageName);

            storage.LoadSensors();

            foreach (ISensor sensor in _storage.Sensors)
            {
                storage.AddSensor(sensor);
            }

            storage.Save();
        }

        public void Dispose()
        {
            UnbindStorage();
            _storage.Dispose();
        }

        private ISensor CreateSensor(string type, int begin, int end)
        {
            ISensor sensor = null;
            Interval interval = new(begin, end);

            switch (type)
            {
                case "Temperature":
                    sensor = new TemperatureSensor(interval);
                    break;

                case "Humidity":
                    sensor = new HumiditySensor(interval);
                    break;

                case "Pressure":
                    sensor = new PressureSensor(interval);
                    break;
            }

            return sensor;
        }

        private void BindStorageWith(DataGridView dataContainer)
        {
            dataContainer.DataSource = _storage.Sensors;
            _unsubscribers = new();

            for (int i = 0; i < _storage.Sensors.Count; i++)
            {
                _unsubscribers.Add(SubscribeToSensor(
                    _storage.Sensors[i], 
                    dataContainer["Value", i]));
            }
        }

        private IDisposable SubscribeToSensor(ISensor sensor, DataGridViewCell cell)
        {
            Subscriber subscriber = new(cell);
            return sensor.Subscribe(subscriber);
        }

        private void UnbindStorage()
        {
            if (_unsubscribers is not null)
            {
                foreach (IDisposable unsubscriber in _unsubscribers)
                {
                    unsubscriber.Dispose();
                }
            }
        }
    }
}
