﻿using System;
using System.Windows.Forms;

namespace SensorsApp.Controller
{
    /// <summary>
    /// Sensor new value watcher.
    /// </summary>
    public class Subscriber : IObserver<int>
    {
        private readonly DataGridViewCell _cell;

        /// <summary>
        /// Constructor, sets the cell to update when observed event rises.
        /// </summary>
        /// <exception cref="ArgumentNullException">Thrown when cell is null.</exception>
        public Subscriber(DataGridViewCell cell)
        {
            _cell = cell ?? 
                    throw new ArgumentNullException(
                        "Can't update null reference cell.");
        }

        public void OnCompleted() { }

        public void OnError(Exception error) { }

        /// <summary>
        /// Renew sensor list entry.
        /// </summary>
        /// <param name="value">New value of sensor.</param>
        public void OnNext(int value)
        {
            _cell.Value = value;
            _cell.DataGridView?.EndEdit();
            _cell.DataGridView?.InvalidateCell(_cell);
        }
    }
}
