using System;
using System.Threading;
using System.Windows.Forms;
using SensorsApp.Controller;
using SensorsApp.Model;
using SensorsApp.Model.Base;
using SensorsApp.View;

namespace SensorsApp
{
    static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
