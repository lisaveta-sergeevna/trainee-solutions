﻿using System;
using System.Collections.Generic;
using System.IO;
using NLog;

namespace MonitoringApp
{
    /// <summary>
    /// Monitor the availability of web sites, mentioned in track file, asynchronously.
    /// </summary>
    public class WebManager : IDisposable
    {
        private readonly FileSystemWatcher _fileWatcher;

        private readonly Logger _log;

        private readonly MailAdministrator _mailAdmin;

        private List<WebMonitor> _monitors;

        private bool _isDisposed;

        /// <summary>
        /// Constructor of the monitor, checks for number of monitor instances.
        /// Recommended to use LoadSettings after WebManager creation to initiate work.
        /// Can throw FileSystemWatcher exceptions due to inner work with it.
        /// Can throw Mutex creation exceptions.
        /// </summary>
        /// <exception cref="ArgumentException">Thrown when fileWatcher is null,
        /// or when admin is null.</exception>
        public WebManager(FileSystemWatcher fileWatcher, MailAdministrator admin)
        {
            if (fileWatcher is null)
            {
                throw new ArgumentException("Can't work with null" +
                                            " or empty file.");
            }

            if (admin is null)
            {
                throw new ArgumentException("Can't configure mail" +
                                            " administrator cause it's null" +
                                            " reference object.");
            }

            _log = LogManager
                .LoadConfiguration("NLog.config")
                .GetCurrentClassLogger();

            _mailAdmin = admin;
            _fileWatcher = fileWatcher;

            _fileWatcher.Changed += OnFileChanged;
            _fileWatcher.EnableRaisingEvents = true;

            _isDisposed = false;
        }

        /// <summary>
        /// Load site settings to monitor to.
        /// Can throw StreamReader exceptions due to inner file reading.
        /// Can throw FormatException due to inner file parsing.
        /// </summary>
        public void LoadSettings()
        {
            _monitors = new();

            using StreamReader reader = new(_fileWatcher.Filter);

            string line;

            while ((line = reader.ReadLine()) is not null)
            {
                SiteSettings settings = SiteSettings.Parse(line);
                WebMonitor monitor = new(settings, _mailAdmin, _log);

                _monitors.Add(monitor);
                monitor.Start();
            }
        }

        /// <summary>
        /// Settings reloading method, called when file changed.
        /// </summary>
        public void ReloadSettings()
        {
            foreach (WebMonitor monitor in _monitors)
            {
                monitor.Stop();
            }

            LoadSettings();
        }

        /// <summary>
        /// Close resources of WebManager.
        /// </summary>
        public void Dispose()
        {
            if (!_isDisposed)
            {
                foreach (WebMonitor monitor in _monitors)
                {
                    monitor.Dispose();
                }

                _fileWatcher.EnableRaisingEvents = false;
                _isDisposed = true;
            }
        }

        private void OnFileChanged(object sender, FileSystemEventArgs e)
        {
            if (e.ChangeType == WatcherChangeTypes.Changed ||
                e.ChangeType == WatcherChangeTypes.Created)
            {
                ReloadSettings();
            }
        }
    }
}
