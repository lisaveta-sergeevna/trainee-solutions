﻿using System;
using System.Net.Mail;

namespace MonitoringApp
{
    /// <summary>
    /// Describes site check settings for WebManager.
    /// </summary>
    public readonly struct SiteSettings
    {
        /// <summary>
        /// Interval of checking the access, milliseconds.
        /// </summary>
        public int CheckInterval { get; }

        /// <summary>
        /// Max server response delay time, milliseconds.
        /// </summary>
        public int MaxDelayTime { get; }

        /// <summary>
        /// Uri of site to check to.
        /// </summary>
        public Uri SiteUri { get; }

        /// <summary>
        /// E-mail address of site admin to send reports.
        /// </summary>
        public MailAddress ReceiverMail { get; }

        /// <summary>
        /// Constructs site settings.
        /// </summary>
        /// <param name="checkInterval">Interval of checking the access, milliseconds.</param>
        /// <param name="maxDelay">Max server response delay time, milliseconds.</param>
        /// <param name="siteUri">Uri of site to check to.</param>
        /// <param name="receiverMail">E-mail address of site admin to send reports.</param>
        /// <exception cref="ArgumentException">Thrown when checkInterval or maxDelay are negative values,
        /// when siteUri or receiverMail are null or empty.</exception>
        public SiteSettings(int checkInterval, int maxDelay, 
            string siteUri, string receiverMail)
        {
            if (checkInterval < 0 || maxDelay < 0)
            {
                throw new ArgumentException("Check interval" +
                                            " and server delay should be" +
                                            " positive values in milliseconds.");
            }

            if (string.IsNullOrEmpty(siteUri))
            {
                throw new ArgumentException("Can't reach site with" +
                                            " null or empty address string.");
            }

            if (string.IsNullOrEmpty(receiverMail))
            {
                throw new ArgumentException("Can't report info to" +
                                            " site admin, admin mail is null or empty.");
            }

            CheckInterval = checkInterval;
            MaxDelayTime = maxDelay;
            SiteUri = new(siteUri);
            ReceiverMail = new(receiverMail);
        }

        /// <summary>
        /// Site settings parser.
        /// </summary>
        /// <param name="line">Line to parse settings from.</param>
        /// <returns>Instance of site settings parsed from line.</returns>
        /// <exception cref="FormatException">Thrown when line has
        /// incorrect format and can't be parsed.</exception>
        public static SiteSettings Parse(string line)
        {
            string[] parts = line.Split(' ');

            if (parts.Length != 4)
            {
                throw new FormatException("Line has incorrect" +
                                          " format to parse settings.");
            }

            return new(
                int.Parse(parts[0]), 
                int.Parse(parts[1]),
                parts[2], 
                parts[3]);
        }
    }
}
