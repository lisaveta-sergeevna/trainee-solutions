﻿using System;
using System.Net;
using System.Threading;
using NLog;

namespace MonitoringApp
{
    /// <summary>
    /// Web site availability monitor.
    /// </summary>
    public class WebMonitor : IDisposable
    {
        private Timer _timer;

        private readonly Logger _log;

        /// <summary>
        /// Settings of site to monitor to.
        /// </summary>
        public SiteSettings Settings { get; }

        /// <summary>
        /// Mail administrator to send mails about site state.
        /// </summary>
        public MailAdministrator Administrator { get; }

        /// <summary>
        /// Constructs WebMonitor, doesn't start it.
        /// </summary>
        /// <param name="settings">Settings of the site to monitor to.</param>
        /// <param name="mailAdmin">Mail administrator for report sending.</param>
        /// <param name="logger">Error logger, if null, monitor constructs his own logger.</param>
        /// <exception cref="ArgumentNullException">Thrown when mail admin is null.</exception>
        public WebMonitor(SiteSettings settings, MailAdministrator mailAdmin,
            Logger logger = null)
        {
            if (mailAdmin is null)
            {
                throw new ArgumentNullException("MailAdmin admin " +
                                                "can't be null.");
            }

            if (logger is null)
            {
                _log = LogManager.LoadConfiguration("NLog.config")
                    .GetCurrentClassLogger();
            }
            else
            {
                _log = logger;
            }

            Settings = settings;
            Administrator = mailAdmin;
        }

        /// <summary>
        /// Start monitoring web resource.
        /// </summary>
        public void Start()
        {
            _timer = new Timer(MonitorSiteAsync, null,
                0, Settings.CheckInterval * 1000);
        }

        /// <summary>
        /// Stop monitoring web resource.
        /// </summary>
        public void Stop()
        {
            _timer?.Dispose();
        }

        /// <summary>
        /// Dispose web monitor object for correct work cancellation.
        /// </summary>
        public void Dispose()
        {
            Stop();
        }

        private async void MonitorSiteAsync(object obj)
        {
            WebRequest request = WebRequest.Create(Settings.SiteUri);
            request.Timeout = Settings.MaxDelayTime * 1000;

            try
            {
                using WebResponse response = await request.GetResponseAsync();

                lock (_log)
                {
                    _log.Info($"Site \"{Settings.SiteUri}\" is available. " +
                              $"Content type: {response.ContentType}. " +
                              $"Length: {response.ContentLength}");
                }

                return;
            }
            catch (Exception ex)
            {
                lock (_log)
                {
                    _log.Error(ex, $"Site \"{Settings.SiteUri}\" " +
                                   $"was unavailable, try to send notification.");
                }
            }

            try
            {
                await Administrator.SendMailNotificationAsync(
                    Settings.ReceiverMail, Settings.SiteUri);
            }
            catch (Exception ex)
            {
                lock (_log)
                {
                    _log.Error(ex, $"En error occurred while sending th report.");
                }
            }
        }
    }
}
