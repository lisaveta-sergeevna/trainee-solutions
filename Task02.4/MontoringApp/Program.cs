﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace MonitoringApp
{
    internal class Program
    {
        private static readonly string _monitorMutexName =
            "Global\\MonitoringAppMutex16042021";

        private static Mutex _mutex;

        private static bool IsSingleInstance()
        {
            try
            {
                Mutex.OpenExisting(_monitorMutexName);
            }
            catch
            {
                _mutex = new(true, _monitorMutexName);
                return true;
            }

            return false;
        }

        internal static void Main(string[] args)
        {
            if (!IsSingleInstance())
            {
                return;
            }

            const string file = "sites.txt";

            try
            {
                using MailAdministrator admin = new("smtp.gmail.com",
                    587, new("user@gmail.com", "password"));

                using FileSystemWatcher fileWatcher = new(
                    Directory.GetCurrentDirectory(), file);

                using WebManager manager = new(fileWatcher, admin);

                manager.LoadSettings();

                File.AppendAllLines(file, new List<string>
                {
                    "10 5 https://google.com/ johnny.doe.post@yandex.by"
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                _mutex?.Close();
            }
        }
    }
}
