﻿using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace MonitoringApp
{
    /// <summary>
    /// Async receiverMail reports sender for WebManager needs.
    /// </summary>
    public class MailAdministrator : IDisposable
    {
        private SmtpClient _client;

        private readonly NetworkCredential _reporterCredentials;

        private bool _isDisposed;

        /// <summary>
        /// Name of the host to connect for sending receiverMail via SMTP.
        /// </summary>
        public string Host { get; }

        /// <summary>
        /// Port to connect receiverMail administrator to.
        /// </summary>
        public int Port { get; }

        /// <summary>
        /// Constructor with the checks.
        /// </summary>
        /// <param name="host">SMTP host for receiverMail administrator.</param>
        /// <param name="port">Port to connect to.</param>
        /// <param name="credentials">User and password data for administrator account.</param>
        /// <exception cref="ArgumentException">Thrown when credentials are null.</exception>
        public MailAdministrator(string host, int port, NetworkCredential credentials)
        {
            if (credentials is null)
            {
                throw new ArgumentException("Can't instantiate receiverMail" +
                                            " administrator without network credentials.");
            }

            _isDisposed = false;

            Host = host;
            Port = port;

            _reporterCredentials = credentials;
            _client = new(Host, Port)
            {
                Credentials = _reporterCredentials,
                EnableSsl = true
            };
        }

        /// <summary>
        /// Async notification report sending.
        /// </summary>
        /// <param name="receiverMail">Mail to send report to.</param>
        /// <param name="uri">Uri of the monitored site.</param>
        /// <exception cref="ArgumentNullException">Thrown when receiverMail or uro is null.</exception>
        public async Task SendMailNotificationAsync(MailAddress receiverMail, Uri uri)
        {
            if (receiverMail is null)
            {
                throw new ArgumentNullException("Can't send notification" +
                                                " when receiverMail is null.");
            }

            if (uri is null)
            {
                throw new ArgumentNullException("Can't do the report" +
                                                " about null value site uri.");
            }

            if (_isDisposed)
            {
                _isDisposed = false;
                _client = new(Host, Port)
                {
                    Credentials = _reporterCredentials,
                    EnableSsl = true
                };
            }

            MailMessage message = new()
            {
                From = new MailAddress(_reporterCredentials.UserName),
                To = { receiverMail },
                Subject = "Notification from monitoring system",
                Body = $"Hello, dear client!\n" +
                       $"Your site \"{uri}\" was unavailable for monitoring on {DateTime.Now:F}.\n" +
                       $"Please, fix that problem.\n\n" +
                       $"Sincerely yours,\n" +
                       $"WebManager administrator.\n"
            };

            await _client.SendMailAsync(message);
        }

        /// <summary>
        /// Release CLR resources used for network connection by receiverMail administrator.
        /// </summary>
        public void Dispose()
        {
            if (!_isDisposed)
            {
                _client?.Dispose();
                _isDisposed = true;
            }
        }
    }
}
