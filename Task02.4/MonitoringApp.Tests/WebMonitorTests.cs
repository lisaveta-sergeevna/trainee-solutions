﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MonitoringApp.Tests
{
    [TestClass]
    public class WebMonitorTests
    {
        private static readonly string _site = @"https://docs.microsoft.com/";
        private static readonly string _mail = @"user@_mail.ru";

        private readonly SiteSettings _settings 
            = new(10, 30, _site, _mail);

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Constructor_WhenMailAdminIsNull_ThrowException()
        {
            new WebMonitor(_settings, null);
        }
    }
}
