﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MonitoringApp.Tests
{
    [TestClass]
    public class WebManagerTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Constructor_WhenDataIsNotValid_ThrowException()
        {
            using MailAdministrator administrator = new(
                "smtp.gmail.com",
                587,
                new("noreply@gmail.com", "password"));

            new WebManager(null, administrator);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Constructor_WhenAdminIsNull_ThrowException()
        {
            using FileSystemWatcher fileWatcher = new(
                Directory.GetCurrentDirectory(), "site.txt");

            new WebManager(fileWatcher, null);
        }
    }
}
