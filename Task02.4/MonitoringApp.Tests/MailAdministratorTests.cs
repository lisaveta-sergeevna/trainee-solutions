﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MonitoringApp.Tests
{
    [TestClass]
    public class MailAdministratorTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Constructor_WhenCredentialsAreNull_ThrowException()
        {
            new MailAdministrator("valid host", 8041, null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task SendMailNotificationAsync_WhenMailIsNull_ThrowException()
        {
            using MailAdministrator administrator = new(
                "smtp.gmail.com",
                587,
                new("noreply@gmail.com", "password"));

            await administrator.SendMailNotificationAsync(null, new("https://docs.microsoft.com/"));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task SendMailNotificationAsync_WhenUriIsNull_ThrowException()
        {
            using MailAdministrator administrator = new(
                "smtp.gmail.com",
                587,
                new("noreply@gmail.com", "password"));

            await administrator.SendMailNotificationAsync(new("example@gmail.com"), null);
        }
    }
}
