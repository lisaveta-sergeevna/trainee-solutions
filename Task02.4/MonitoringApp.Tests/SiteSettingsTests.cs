using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MonitoringApp.Tests
{
    [TestClass]
    public class SiteSettingsTests
    {
        [DataTestMethod]
        [DataRow(-10, 30, @"https://docs.microsoft.com/", @"user@mail.ru")]
        [DataRow(10, -30, @"https://docs.microsoft.com/", @"user@mail.ru")]
        [DataRow(10, 30, null, @"user@mail.ru")]
        [DataRow(10, 30, "", @"user@mail.ru")]
        [DataRow(10, 30, @"https://docs.microsoft.com/", null)]
        [DataRow(10, 30, @"https://docs.microsoft.com/", "")]
        [ExpectedException(typeof(ArgumentException))]
        public void Constructor_WhenDataIsInvalid_ThrowException(int checkInterval, 
            int maxDelay, string siteUri, string adminMail)
        {
            new SiteSettings(checkInterval, maxDelay, siteUri, adminMail);
        }

        [TestMethod]
        [ExpectedException(typeof(FormatException))]
        public void Parse_WhenLineHasWrongFormat_ThrowException()
        {
            SiteSettings.Parse("wrong formatted line");
        }

        [TestMethod]
        public void Constructor_WhenDataIsValid_InstantiateSiteSettings()
        {
            string site = @"https://docs.microsoft.com/";
            string mail = @"user@mail.ru";
            SiteSettings settings = new(10, 30, site, mail);

            Assert.AreEqual(site, settings.SiteUri.AbsoluteUri);
            Assert.AreEqual(mail, settings.ReceiverMail.Address);
        }

        [TestMethod]
        public void Parse_WhenLineHasCorrectFormat_InstantiateSiteSettings()
        {
            long checkInterval = 10;
            long maxDelay = 30;
            string site = @"https://docs.microsoft.com/";
            string mail = @"user@mail.ru";

            SiteSettings settings = SiteSettings.Parse($"{checkInterval} " +
                                                       $"{maxDelay} {site} {mail}");

            Assert.AreEqual(checkInterval, settings.CheckInterval);
            Assert.AreEqual(maxDelay, settings.MaxDelayTime);
            Assert.AreEqual(site, settings.SiteUri.AbsoluteUri);
            Assert.AreEqual(mail, settings.ReceiverMail.Address);
        }
    }
}
